<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected function category_name(){
        return $this->belongsTo('App\GalleryCategory', 'category');
    }
}
