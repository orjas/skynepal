<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [

            'title' => 'required',
            'description' => 'required'

        ],
            [
                'required' => 'This field is required'
            ]);


        $about = new About();
        $about->title = $request->title;
        $about->category = $request->category;
        $about->description = $request->description;
        $about->contact = ($request->contact) == 'on' ? '1' : '0';
        $about->registration = ($request->registration) == 'on' ? '1' : '0';

        $about->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/about');
    }

    public function view()
    {
        $abouts = About::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.about.view', compact('abouts'));
    }

//
    public function delete($id)
    {
        $about = About::findOrfail($id);


        $about->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/about');
    }

    public function edit($id)
    {
        $about = About::findOrfail($id);
        return view('admin.about.edit', compact('about'));
    }

    public function update($id, Request $request)
    {

        $this->validate($request, [

            'title' => 'required',
            'description' => 'required'

        ],
            [
                'required' => 'This field is required'
            ]);


        $about = About::findOrfail($id);
        $about->title = $request->title;
        $about->category = $request->category;
        $about->description = $request->description;
        $about->contact = ($request->contact) == 'on' ? '1' : '0';
        $about->registration = ($request->registration) == 'on' ? '1' : '0';

        $about->save();
        $request->session()->flash('alert-success', 'Successful updated');

        return redirect('admin/about');
    }
}