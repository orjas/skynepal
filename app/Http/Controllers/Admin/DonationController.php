<?php

namespace App\Http\Controllers\Admin;

use App\Model\Donote;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DonationController extends Controller
{
    public function index()
    {
        $donation=Donote::first();

        if(!$donation){
            $donation=new Donote();
        }

        return view('admin.donation.edit',compact('donation'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'description'=>'required'
        ]);
        if($request->id){
            $donation=Donote::findOrFail($request->id);

        }else{
            $donation=new Donote();
        }
        $donation->description=$request->description;
        $donation->save();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Donation Record successfully changed!');
        return redirect()->back();

    }
}
