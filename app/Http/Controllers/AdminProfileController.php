<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminProfileController extends Controller
{
    public function update(Request $request)
    {

        $this->validate($request, [
            'name' => 'min:4',
            'email' => 'email',
            'password' => 'min:8'
        ]);
        $id = Auth::user()->id;
        $check = 0;

        $user = User::findOrFail($id);
        $user->name = $request->name;
        if ($request->has('email') && $request->email != $user->email) {

            $user->email = $request->email;
            $check = 1;
        }
        if ($request->has('password')) {
            if ($request->password != $request->confirm_password) {
                $request->session()->flash('confirm','Password Donot Match.Please Retry');

                return back();


            } else {
                $user->password = bcrypt($request->password);
                $check = 1;
            }

        }

      $user->save();
        if($check==0){
            $request->session()->flash('alert-success', 'Successful Updated!');

            return redirect('admin/adminprofile');
        }
        else{
            $request->session()->flash('alert-success', 'Successfully updated!Please login with new Credential');

            return redirect('auth/logout');
        }

    }
}