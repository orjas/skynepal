<?php

namespace App\Http\Controllers;

use App\Model\Blog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{


    public function view()
    {
        $blogs=Blog::orderBy('created_at','desc')->paginate(10);
        return view('admin.blogs.view',compact('blogs'));
    }

    public function delete($id)
    {
       $blog=Blog::findOrfail($id);
       $blog->delete();
       \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');
        return redirect('admin/blogs');
    }

    public function status_change(Request $request)
    {
        $blog=Blog::findOrFail($request->id);
        $blog->status=$blog->status?0:1;
        $blog->save();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Blog status successfully changed!');
        return redirect()->back();


    }
    public function edit($id)
    {
        $blogs=Blog::findOrfail($id);
        return view ('admin.blogs.edit',compact('blogs'));

    }


}
