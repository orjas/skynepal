<?php

namespace App\Http\Controllers;

use App\GalleryCategory;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    //
    public function store(Request $request )
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png|required',



        ]);


        $category=new GalleryCategory();
        $category->name=$request->name;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }


            $location ='images/gallery/' . $name;

            Image::make($file)->resize(300,150)->save($location);
            $category->image=$name;
        }

        $category->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/category');
    }
    public function view()
    {
        $categories=GalleryCategory::orderBy('created_at','desc')->paginate(10);
        return view('admin.category.view',compact('categories'));
    }
    public function delete($id)
    {
        $gallerys=GalleryCategory::findOrfail($id);

        \File::delete('assest/images/gallery/'.$gallerys->image);
        $gallerys->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/category');
    }
    public function edit($id){
        $category=GalleryCategory::findOrfail($id);

        return view('admin.category.edit',compact('category'));
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'image|mimes:jpeg,png',



        ]);

        $category=GalleryCategory::findOrfail($id);
        $category->name=$request->name;
        if($request->hasFile('image')){
            \File::delete('assest/images/gallery/'.$category->image);

            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }


            $location ='images/gallery/' . $name;

            Image::make($file)->resize(300,150)->save($location);
            $category->image=$name;
        }
        $category->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/category');
    }
}
