<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class FAQController extends Controller
{
    //
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required|min:6',

        ]);
        $question=new Question();
        $question->title=$request->title;
        $question->description=$request->description;



        $question->save();
        $request->session()->flash('alert-success', 'Successful added!');
        return redirect('admin/faq');
    }

    public function view()
    {

        $questions=Question::orderBy('created_at','desc')->paginate(10);

        return view('admin.FAQ.view',compact('questions'));
    }

    public function delete($id)
    {
        $question=Question::findOrfail($id);

        $question->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/faq');
    }

    public function edit($id)
    {
        $question=Question::find($id);
        return view('admin.faq.edit',compact('question'));
    }


    public function update($id,Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',




        ]);
        $question=Question::findOrfail($id);
        $question->title=$request->title;
        $question->description=$request->description;


        $question->save();

        $request->session()->flash('alert-success', 'Successful Updated!');
        return redirect('admin/faq');
    }
}
