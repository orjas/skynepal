<?php

namespace App\Http\Controllers;

use App\File;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'file' => 'mimes:doc,docx,pdf|required',


        ]);



        $f = new File();
        $f->name = $request->name;
        if ($request->hasFile('file')) {
            $fname=$request->file;
            $ext=$fname->getClientOriginalExtension();

          $name=chr(rand(97,122)).random_int(1000,9999).'.'.$ext;




            $location ="file/";

            $fname->move($location,$name);
            if($fname->getClientOriginalExtension()=='docx'){
                $f->extension='word';
            }
            elseif($fname->getClientOriginalExtension()=='xls'){
                $f->extension='excel';
            }
            else {
                $f->extension = $fname->getClientOriginalExtension();
            }
                $f->file = $name;
        }

        $f->hits=0;
        $f->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/file');
    }


    public function view()
    {
        $files=File::orderBy('created_at','desc')->paginate(10);
        return view('admin.file.view',compact('files'));
    }
    public function delete($id)
    {
        $file=File::findOrfail($id);

        \File::delete('file/'.$file->file);
        $file->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/file');
    }
}