<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\GalleryCategory;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    //
    public function index()
    {
        $categories=GalleryCategory::lists('name','id');

        return view('admin.gallery.add',compact('categories'));
    }

    public function store(Request $request )
    {
$this->validate($request, [
'image' => 'image|mimes:jpeg,png|required',


]);

$gallery=new Gallery();
$gallery->category=$request->category;
$gallery->description=$request->description;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());
            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }


            $location ='images/gallery/' . $name;

            Image::make($file)->resize(300,150)->save($location);
            $gallery->image=$name;
        }
    $gallery->save();
    $request->session()
    ->flash('alert-success', 'Successful Added!');

return redirect('admin/gallery');
}
public function view()
{
    $gallerys=Gallery::orderBy('created_at','desc')->paginate(10);
    return view('admin.gallery.view',compact('gallerys'));
}

public function delete($id)
{
    $gallerys=Gallery::findOrfail($id);

    \File::delete('assest/images/gallery/'.$gallerys->image);
    $gallerys->delete();
    \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

    return redirect('/admin/gallery');
}
public function edit($id)
{
    $gallerys=Gallery::find($id);
    $categories=GalleryCategory::lists('name','id');


    return view('admin.gallery.edit',compact('gallerys','categories'));
}

public function update($id,Request $request)
{
    $this->validate($request, [

        'image'=>'image|mimes:jpeg,png',


    ]);
    $gallerys=Gallery::find($id);
    $gallerys->category=$request->category;
    $gallerys->description=$request->description;

    if($request->hasFile('image')){

        \File::delete('assest/images/gallery/'.$gallerys->image);
        $file=$request->image;
        $ext=$file->getClientOriginalExtension();


        $name=str_replace(" ","",$file->getClientOriginalName());

        if(strlen($name)>25){
            $output=substr($name,0,20);
            $name=$output.random_int(1000,9999).'.'.$ext;
        }else if(strlen($name)>15){
            $name=chr(rand(97,122)).random_int(1000,9999).$name;
        }
        else{
            $name=uniqid().$name;
        }


        $location ='images/gallery/' . $name;

        Image::make($file)->resize(300,150)->save($location);
        $gallerys->image=$name;
    }
    $gallerys->save();
    $request->session()->flash('alert-success', 'Successful Updated!');

    return redirect('admin/gallery');
}
}
