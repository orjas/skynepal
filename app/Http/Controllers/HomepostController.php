<?php

namespace App\Http\Controllers;

use App\HomepagePost;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class HomepostController extends Controller
{
    public function store(Request $request )
    {
$this->validate($request, [
'title' => 'required',
'description' => 'required|min:6',

'image' => 'required|image|mimes:jpeg,png',
]);
$homepost=new HomepagePost();
$homepost->title=$request->title;
$homepost->category=$request->category;
$homepost->description=$request->description;
if($request->hasFile('image')){
    $file=$request->image;
    $ext=$file->getClientOriginalExtension();


    $name=str_replace(" ","",$file->getClientOriginalName());

    if(strlen($name)>25){
        $output=substr($name,0,20);
        $name=$output.random_int(1000,9999).'.'.$ext;
    }else if(strlen($name)>15){
        $name=chr(rand(97,122)).random_int(1000,9999).$name;
    }
    else{
        $name=uniqid().$name;
    }
$location ='images/homepost/' . $name;

Image::make($file)->resize(450,300)->save($location);
$homepost->image=$name;
}
$homepost->save();
$request->session()->flash('alert-success', 'Successful Added!');

return redirect('admin/homepost');
}
public function view()
{
    $homeposts=HomepagePost::orderBy('created_at','desc')->paginate(10);
    return view('admin.homepost.view',compact('homeposts'));
}

public function delete($id)
{
    $homepost=HomepagePost::findOrfail($id);

    \File::delete('images/homepost/'.$homepost->image);
    $homepost->delete();
    \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

    return redirect('/admin/homepost');
}
public function edit($id)
{
    $homepost=HomepagePost::find($id);
    return view('admin.homepost.edit',compact('homepost'));
}

public function update($id,Request $request)
{
    $this->validate($request, [
        'title' => 'required',
        'description' => 'required|min:6',




    ]);
    $homepost=HomepagePost::findOrfail($id);


    $homepost->title=$request->title;
    $homepost->category=$request->category;
    $homepost->description=$request->description;
    if($request->hasFile('image')){

        \File::delete('images/homepost/'.$homepost->image);
        $file=$request->image;
        $ext=$file->getClientOriginalExtension();


        $name=str_replace(" ","",$file->getClientOriginalName());

        if(strlen($name)>25){
            $output=substr($name,0,20);
            $name=$output.random_int(1000,9999).'.'.$ext;
        }else if(strlen($name)>15){
            $name=chr(rand(97,122)).random_int(1000,9999).$name;
        }
        else{
            $name=uniqid().$name;
        }
        $location ='images/homepost/' . $name;

        Image::make($file)->resize(450,300)->save($location);
        $homepost->image=$name;
    }
    $homepost->save();
    $request->session()->flash('alert-success', 'Successful Updated!');

    return redirect('admin/homepost');
}
}
