<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    //
    public function store(Request $request )
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required|min:6',
            'short_description'=>'required',

             'date'=>'required',
        ]);
        $news=new News();
        $news->title=$request->title;
        $news->category=$request->category;
        $news->description=$request->description;
        $news->short_description=$request->short_description;
        $news->date=$request->date;

        $news->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/news');
    }
    public function view()
    {
        $newses=News::orderBy('date','desc')->paginate(10);
        return view('admin.news.view',compact('newses'));
    }

    public function delete($id)
    {
        $news=News::findOrfail($id);


        $news->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/news');
    }
    public function edit($id)
    {
        $news=News::find($id);
        return view('admin.news.edit',compact('news'));
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required|min:6',
           'short_description'=>'required',
            'date'=>'required'



        ]);

        $news=News::findOrfail($id);


        $news->title=$request->title;
        $news->category=$request->category;
        $news->description=$request->description;
        $news->short_description=$request->short_description;
        $news->date=$request->date;

//        if($request->hasFile('image')){
//            $this->validate($request,[
//            ]);
//            $file=$request->image;
//            $name=$file->getClientOriginalName();
//
//            if(strlen($name)>15){
//                $output=str_limit($name,10);
//                $id=$output.rand(1000,9999).'.png';
//            }
//            else{
//                $id=$name.rand(1000,9999).'.png';
//
//            }
//            $location = public_path() . '\images\news\\' . $id;
//
//            Image::make($file)->resize(450,300)->encode('png')->save($location);
//            $news->image=$id;
//        }
        $news->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/news');
    }
}
