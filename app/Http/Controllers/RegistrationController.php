<?php

namespace App\Http\Controllers;

use App\Registration;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class RegistrationController extends Controller
{
    public function store(Request $request)
    {

        $this->validate($request,[
           'name'=>'required',
            'course'=>'required',
            'phone'=>'required',
            'age'=>'required',
            'address'=>'required',
            'g-recaptcha-response' => 'required|captcha'
        ],
            [
                'required'=>'This field is required',
                'g-recaptcha-response' => [
                    'required' => 'Please verify that you are not a robot.',
                    'captcha' => 'Captcha error! try again later or contact site admin.',
                ]]);
        $registration=new Registration();
        $registration->course=json_encode($request->course);
        $registration->name=$request->name;
        $registration->email=$request->email;
        $registration->phone=$request->phone;
        $registration->gender=$request->gender;
        $registration->age=$request->age;
        $registration->education=$request->education;
        $registration->address=$request->address;
        $registration->profession=$request->profession;

        $registration->physical=$request->physical;
        $registration->recent_course=$request->recent_course;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location = 'images/registration/' . $name;

            Image::make($file)->save($location);
            $registration->image=$name;
        }

     $registration->save();
        $request->session()->flash('alert-success', 'Registration request sent .We will soon responsd!');

        return redirect()->back();
    }

    public function index()

    {
        $registrations=Registration::orderBy('created_at','desc')->paginate(10);
        return view('admin.registration',compact('registrations'));

    }

    public function delete($id)
    {


        $registration=Registration::findOrfail($id);

        \File::delete('images/registration/' . $registration->image);
        $registration->delete();

        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/registration');
    }
}
