<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class SettingController extends Controller
{
    public function view()
    {
        $setting=Setting::first();
        return view('admin.setting.view',compact('setting'));
    }


    public function edit($id)
    {
        $setting=Setting::findOrfail($id);
        return view('admin.setting.edit',compact('setting'));
    }

    public function update($id,Request $request)
    {

        $this->validate($request, [
            'logo' => '|image|mimes:jpeg,png',
            'title' => 'required|min:6',
            'subtitle' => 'required|min:6',
            'description' => 'required|min:6',
            'email' => 'required|',
            'phone' => 'required|',
            'address'=>'required',
            'message'=>'required'


        ],
            [
                'required'=>'This field is required'
            ]);

        $setting=Setting::findOrfail($id);
        $setting->title=$request->title;
        $setting->subtitle=$request->subtitle;
        $setting->description=$request->description;
        $setting->email=$request->email;
        $setting->message=$request->message;
        $setting->phone=$request->phone;
        $setting->address=$request->address;
        $setting->facebook=$request->facebook;
        if($request->hasFile('logo')){
            \File::delete('images/logo/' . $setting->image);

            $file=$request->logo;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }
            $location ='images/logo/' . $name;

            Image::make($file)->save($location);
            $setting->logo=$name;
        }
        $setting->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('/admin/setting');

    }
}
