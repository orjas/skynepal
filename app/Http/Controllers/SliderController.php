<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    //
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png|required',
            'title' => 'required|min:6',
            'subtitle'=>'required|min:6'

        ],
        [
            'required'=>'This field is required'
            ]);

       $slider=new Slider();
        $slider->title=$request->title;
        $slider->subtitle=$request->subtitle;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }
            $location ='images/slider/' . $name;

            Image::make($file)->resize(730,228)->save($location);
            $slider->image=$name;
        }
        $slider->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/slider');
    }

    public function view()
    {
        $sliders=Slider::orderBy('created_at','desc')->paginate(10);
        return view('admin.slider.view',compact('sliders'));
    }

    public function delete($id)
    {
        $slider=Slider::findOrfail($id);

        \File::delete('images/slider/'.$slider->image);
        $slider->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/slider');
    }
    public function edit($id)
    {
        $slider=Slider::findOrfail($id);
        return view('admin.slider.edit',compact('slider'));
    }

    public function update($id,Request $request)
    {

        $this->validate($request, [

            'title' => 'required|min:6',
            'subtitle'=>'required|min:6'

        ],
            [
                'required'=>'This field is required'
            ]);

        $slider=Slider::findOrfail($id);
        $slider->title=$request->title;
        $slider->subtitle=$request->subtitle;
        if($request->hasFile('image')){

            \File::delete('images/slider/'.$slider->image);
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }
            $location ='images/slider/' . $name;

            Image::make($file)->resize(730,228)->save($location);
            $slider->image=$name;
        }
        $slider->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('/admin/slider');

    }
}
