<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class TeamController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png|required',
            'name' => 'required',
            'position'=>'required'

        ],
            [
                'required'=>'This field is required'
            ]);

        $team=new Team();
        $team->name=$request->name;
        $team->position=$request->position;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }
            $location = 'images/team/' . $name;

            Image::make($file)->save($location);
            $team->image=$name;
        }
        $team->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/team');
    }

    public function view()
    {
        $teams=Team::orderBy('created_at','desc')->paginate(10);
        return view('admin.team.view',compact('teams'));
    }

    public function delete($id)
    {
        $team=Team::findOrfail($id);

        \File::delete('images/team/'.$team->image);
        $team->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/slider');
    }
    public function edit($id)
    {
        $team=Team::findOrfail($id);
        return view('admin.team.edit',compact('team'));
    }

    public function update($id,Request $request)
    {

        $this->validate($request, [
            'image' => 'nullable|image|mimes:jpeg,png',
            'name' => 'required',
            'position'=>'required'

        ],
            [
                'required'=>'This field is required'
            ]);

        $team=Team::findOrfail($id);
        $team->name=$request->name;
        $team->position=$request->position;
        if($request->hasFile('image')){
            \File::delete('images/team/'.$team->image);
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>25){
                $output=substr($name,0,20);
                $name=$output.random_int(1000,9999).'.'.$ext;
            }else if(strlen($name)>15){
                $name=chr(rand(97,122)).random_int(1000,9999).$name;
            }
            else{
                $name=uniqid().$name;
            }
            $location = 'images/team/' . $name;

            Image::make($file)->save($location);
            $team->image=$name;
        }
        $team->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('/admin/team');

    }
}
