<?php

namespace App\Http\Controllers;

use App\Testomonial;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class TestomonialController extends Controller
{
    //
    public function store(Request $request)
    {
$this->validate($request, [
'name' => 'required|min:6|max:255',
'message' => 'required|min:6',
'position' => 'required',

'image' => 'image|mimes:jpeg,png|required',
]);
$test=new Testomonial();
$test->name=$request->name;
$test->position=$request->position;
 $test->category=$request->category;
$test->message=$request->message;

if($request->hasFile('image')){
    $file=$request->image;
    $ext=$file->getClientOriginalExtension();


    $name=str_replace(" ","",$file->getClientOriginalName());

    if(strlen($name)>25){
        $output=substr($name,0,20);
        $name=$output.random_int(1000,9999).'.'.$ext;
    }else if(strlen($name)>15){
        $name=chr(rand(97,122)).random_int(1000,9999).$name;
    }
    else{
        $name=uniqid().$name;
    }
$location = 'images/testomonial/' . $name;

Image::make($file)->save($location);
$test->image=$name;
}
$test->save();
$request->session()->flash('alert-success', 'Successful added!');
return redirect('admin/testomonial');
}

public function view()
{
    $test=Testomonial::orderBy('created_at','desc')->paginate(10);
    return view('admin.testomonial.view',compact('test'));
}

public function delete($id)
{
    $test=Testomonial::findOrfail($id);

    \File::delete('images/testomonial/'.$test->image);
    $test->delete();
    \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

    return redirect('/admin/testomonial');
}


public function edit($id)
{
    $test=Testomonial::find($id);
    return view('admin.testomonial.edit',compact('test'));
}


public function update($id,Request $request)
{
    $this->validate($request, [
        'name' => 'required|min:6|max:255',
        'message' => 'required|min:6',
        'position' => 'required',

        'image'=>'|image|mimes:jpeg,png',


    ]);
    $test=Testomonial::find($id);
    $test->name=$request->name;
    $test->category=$request->category;
    $test->position=$request->position;
    $test->message=$request->message;

    if($request->hasFile('image')){

        \File::delete('images/testomonial/'.$test->image);
        $file=$request->image;
        $ext=$file->getClientOriginalExtension();


        $name=str_replace(" ","",$file->getClientOriginalName());

        if(strlen($name)>25){
            $output=substr($name,0,20);
            $name=$output.random_int(1000,9999).'.'.$ext;
        }else if(strlen($name)>15){
            $name=chr(rand(97,122)).random_int(1000,9999).$name;
        }
        else{
            $name=uniqid().$name;
        }
        $location = 'images/testomonial/' . $name;

        Image::make($file)->save($location);
        $test->image=$name;
    }
    $test->save();
    $request->session()->flash('alert-success', 'Successful Updated!');
    return redirect('admin/testomonial');
}
}
