<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    //
    public function store(Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
            'video' => 'required',

        ]);
        $videos = new Video();
        $videos->category = $request->category;
        $videos->url = $request->video;
        $url = $request->video;
        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
        $id = $matches[1];
        $videos->video = $id;
        $videos->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('/admin/video');
    }

    public function view()
    {
        $videos = Video::orderBy('created_at', 'desc')->paginate(10);
        return view('admin.video.view', compact('videos'));
    }

    public function delete($id)
    {
        $videos = Video::findOrfail($id);

        $videos->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/video');
    }

    public function edit($id)
    {
        $videos = Video::findOrfail($id);
        return view('admin.video.edit', compact('videos'));
    }

    public function update($id, Request $request)
    {
        $this->validate($request, [
            'category' => 'required',
            'video' => 'required',

        ]);
        $videos = Video::findOrfail($id);
        $videos->category = $request->category;
        $videos->url = $request->video;
        if ($request->video) {
            $url = $request->video;
            preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);

            $id = $matches[1];
            $videos->video = $id;

        }

        $videos->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/video');
    }
}