<?php

namespace App\Http\Controllers\frontend;

use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsDisplayController extends Controller
{
    public function news($id)
    {
        $news=News::findOrfail($id);
        return view('newsdefaultpage',compact('news'));

  }

}
