<?php

namespace App\Http\Controllers\frontend;

use App\About;
use App\File;
use App\Gallery;
use App\GalleryCategory;
use App\HomepagePost;
use App\Model\Blog;
use App\Model\Donote;
use App\News;
use App\Question;
use App\Slider;
use App\Team;
use App\Testomonial;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function home(){
        $testomonials=Testomonial::all();

        $homeposts=HomepagePost::all();
        return view('index',compact('testomonials','homeposts'));
    }



    public function abouts($id)
    {

        $about=About::findOrfail($id);
        return view('defaultpage',compact('about'));
    }


    public function faq(){
        $questions=Question::all();
        return view('prasnottar',compact('questions'));
    }

    public function media(){
        $categories=GalleryCategory::all()->sortByDesc('created_at');

        return view('sandarvasamagri.media',compact('categories'));
    }

    public function gallery($id)
    {

        $galleries =Gallery::where('category',$id)->get();
        return view('sandarvasamagri.mediagallery',compact('galleries'));
}
public function video(){
    $videos=Video::all();
    return view('sandarvasamagri.video',compact('videos'));
}

    public function content(){
       $contents=File::all()->sortByDesc('created_at');
        return view('sandarvasamagri.content',compact('contents'));

    }

    public function news()
    {
        $news=News::where('category','News')->orderBy('date','desc')->get();
        return view('gatividi.samachar',compact('news'));
    }

    public function speech()
    {
        $speechs=News::where('category','Speech')->orderBy('date','desc')->get();

        return view('gatividi.mantabya',compact('speechs'));
}


    public function blogs()
    {
        $blogs=Blog::where('status',1)->orderBy('created_at','desc')->get();
        return view('gatividi.blog',compact('blogs'));
        
    }

    public function donation()
    {
        $donation=Donote::first();

        if(!$donation){
            $donation=new Donote();
        }
        return view('donation',compact('donation'));
    }


    public function blog()
    {
        return view('blog.blog');
    }

    public function blog_view($id)
    {
        $blog=Blog::where('status',1)->findOrFail($id);
        return view('blog_view',compact('blog'));

    }

    public function blogSave(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|min:5',
            'email'=>'required|email',
            'phone'=>'required|numeric',
            'address'=>'required|min:5',
            'title'=>'required|min:5',
            'contents'=>'required|min:100',
//        'g-recaptcha-response' => 'required|captcha'
        ], [
            'g-recaptcha-response.required'=>'Please verify that you are not a robot',
            'g-recaptcha-response' => [
                'required' => 'Please verify that you are not a robot.',
                'captcha' => 'Captcha error! try again later or contact site admin.',
            ]]);

        $blog=Blog::create($request->except('token'));

        if($blog){
            $request->session()->flash('alert-success', 'Your blog has been saved. Admin will verify and publish your post soon.!');
        }else{
            $request->session()->flash('alert-success', 'Your post couldnot be submitted. Please try again later.!');

        }
        return redirect()->back();



    }

    public function team()
    {
        $teams=Team::all();
        return view('grihapristha.hamroteam',compact('teams'));
    }

    public function event()
    {
        $events=News::where('category','Event')->orderBy('date','desc')->get();
        return view('awasar.talikaall',compact('events'));
    }
    public function upcoming(){
        date_default_timezone_set('Asia/kathmandu');
        $date=date('Y-m-d');
          $events=News::whereDate('date','>',$date)->where('category','Event')->orderBy('date','desc')->get();

          return view('awasar.talika',compact('events'));
    }

}
