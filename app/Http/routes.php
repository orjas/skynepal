<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','frontend\PageController@home');

Route::get('/about/{id}','frontend\PageController@abouts')->name('about');
Route::get('/opportunity/{id}','frontend\PageController@abouts')->name('opportunity');
Route::get('/participation/{id}','frontend\PageController@abouts')->name('participation');
Route::get('/our-team','frontend\PageController@team')->name('team');


Route::get('/events/all','frontend\PageController@event')->name('events');
Route::get('/events/upcoming','frontend\PageController@upcoming')->name('upcoming');
;






Route::get('/news','frontend\PageController@news')->name('news');
Route::get('/news/{id}','frontend\NewsDisplayController@news')->name('samachar');

Route::get('/speech','frontend\PageController@speech')->name('speech');
Route::get('/speech/{id}','frontend\NewsDisplayController@news')->name('mantabya');



Route::get('blogs','frontend\PageController@blogs')->name('blogs');

Route::get('/blog','frontend\PageController@blog')->name('blog');
Route::post('blog','frontend\PageController@blogSave');
Route::get('blog/{id}','frontend\PageController@blog_view');




Route::get('/media','frontend\PageController@media')->name('media');
Route::get('/media/{id}/gallery','frontend\PageController@gallery')->name('gallery');
Route::get('/media/video','frontend\PageController@video')->name('video');
Route::get('/media/content','frontend\PageController@content')->name('content');
Route::get('/FAQ','frontend\PageController@faq')->name('FAQ');
Route::get('donation','frontend\PageController@donation');


//message
Route::get('/contact-us', function () {
    return view('samparka');
})->name('contact');
Route::post('/contact-us','MessageController@store');
//registration
Route::get('/registration', function () {
    return view('sahavagita.avedan');
})->name('registration');
Route::post('/registration','RegistrationController@store');

Route::any('login',function(){
   abort(404);
});




Route::get('dashboard/login', 'Auth\AuthController@getLogin')->name('login');
Route::post('dashboard/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('logout');


Route::get('/home',function(){
    return redirect('admin/slider');
});

//Admin
Route::group(['middleware'=>'auth'],function(){
//Slider
    Route::get('admin/dashboard',function(){
        return view('admin.dashboard');
    });
Route::get('/admin/slider/add',function(){
    return view('admin.slider.add');
});
Route::post('/admin/slider/add','SliderController@store');
Route::get('/admin/slider','SliderController@view');
Route::get('/admin/slider/{id}/delete','SliderController@delete');
Route::get('/admin/slider/{id}/edit','SliderController@edit');
Route::post('/admin/slider/{id}/update','SliderController@update');

//Setting
Route::get('/admin/setting','SettingController@view');

Route::get('/admin/setting/{id}/edit','SettingController@edit');
Route::post('/admin/setting/{id}/update','SettingController@update');

//About us
Route::get('/admin/aboutus','AboutusController@view');
Route::get('/admin/aboutus/{id}/edit','AboutusController@edit');
Route::post('/admin/aboutus/{id}/update','AboutusController@update');

//Yogprogram
    Route::get('/admin/yogprogram','YogProgramController@edit');
    Route::post('/admin/yogprogram/{id}/update','YogProgramController@update');
    //Policy
    Route::get('/admin/policy','PolicyController@edit');
    Route::post('/admin/policy/{id}/update','PolicyController@update');

    //about
    Route::get('/admin/about/add',function(){
        return view('admin.about.add');
    });
    Route::post('/admin/about/add','AboutController@store');
    Route::get('/admin/about','AboutController@view');
    Route::get('/admin/about/{id}/delete','AboutController@delete');
    Route::get('/admin/about/{id}/edit','AboutController@edit');
    Route::post('/admin/about/{id}/update','AboutController@update');



    Route::get('admin/donation','Admin\DonationController@index');
    Route::post('admin/donation','Admin\DonationController@store');

    //team
    Route::get('/admin/team/add',function(){
        return view('admin.team.add');
    });
    Route::post('/admin/team/add','TeamController@store');
    Route::get('/admin/team','TeamController@view');
    Route::get('/admin/team/{id}/delete','TeamController@delete');
    Route::get('/admin/team/{id}/edit','TeamController@edit');
    Route::post('/admin/team/{id}/update','TeamController@update');

    //testomonial
    Route::get('/admin/testomonial/add',function(){
        return view('admin.testomonial.add');
    });
    Route::post('/admin/testomonial/add','TestomonialController@store');
    Route::get('/admin/testomonial','TestomonialController@view');
    Route::get('/admin/testomonial/{id}/delete','TestomonialController@delete');
    Route::get('/admin/testomonial/{id}/edit','TestomonialController@edit');
    Route::post('/admin/testomonial/{id}/update','TestomonialController@update');

    //message
    Route::get('/admin/message','MessageController@index');
    Route::get('/admin/message/{id}/delete','MessageController@delete');
    //Registration
    Route::get('/admin/registration','RegistrationController@index');
    Route::get('/admin/registration/{id}/delete','RegistrationController@delete');

    //video
    //video routes
    Route::get('/admin/video/add',function(){
        return view('admin.video.add');
    });
    Route::post('/admin/video/add','VideoController@store');
    Route::get('/admin/video','VideoController@view');
    Route::get('/admin/video/{id}/delete','VideoController@delete');
    Route::get('/admin/video/{id}/edit','VideoController@edit');
    Route::post('/admin/video/{id}/update','VideoController@update');

    //gallery

    Route::get('/admin/gallery/add','GalleryController@index');
    Route::post('/admin/gallery/add','GalleryController@store');
    Route::get('/admin/gallery','GalleryController@view');
    Route::get('/admin/gallery/{id}/delete','GalleryController@delete');
    Route::get('/admin/gallery/{id}/edit','GalleryController@edit');
    Route::post('/admin/gallery/{id}/update','GalleryController@update');

    //Gallery Category
    Route::get('/admin/category/add',function(){
        return view('admin.category.add');
    });
    Route::post('/admin/category/add','CategoryController@store');
    Route::get('/admin/category','CategoryController@view');
    Route::get('/admin/category/{id}/delete','CategoryController@delete');
    Route::get('/admin/category/{id}/edit','CategoryController@edit');
    Route::post('/admin/category/{id}/update','CategoryController@update');


//FAQ
    Route::get('/admin/faq/add',function(){
        return view('admin.FAQ.add');
    });
    Route::post('/admin/faq/add','FAQController@store');
    Route::get('/admin/faq','FAQController@view');
    Route::get('/admin/faq/{id}/delete','FAQController@delete');
    Route::get('/admin/faq/{id}/edit','FAQController@edit');
    Route::post('/admin/faq/{id}/update','FAQController@update');
//blogs
    Route::get('admin/blogs/{id}/edit','BlogController@edit');
    Route::get('/admin/blogs','BlogController@view');
    Route::get('/admin/blogs/{id}/delete','BlogController@delete');
    Route::post('admin/blogs','BlogController@status_change');


    //news
    Route::get('/admin/news/add',function(){
        return view('admin.news.add');
    });
    Route::post('/admin/news/add','NewsController@store');
    Route::get('/admin/news','NewsController@view');
    Route::get('/admin/news/{id}/delete','NewsController@delete');
    Route::get('/admin/news/{id}/edit','NewsController@edit');
    Route::post('/admin/news/{id}/update','NewsController@update');

    //homepost
    Route::get('/admin/homepost/add',function(){
        return view('admin.homepost.add');
    });
    Route::post('/admin/homepost/add','HomepostController@store');
    Route::get('/admin/homepost','HomepostController@view');
    Route::get('/admin/homepost/{id}/delete','HomepostController@delete');
    Route::get('/admin/homepost/{id}/edit','HomepostController@edit');
    Route::post('/admin/homepost/{id}/update','HomepostController@update');


//file
Route::get('/admin/file/add',function(){
    return view('admin.file.add');
});
Route::post('/admin/file/add','FileController@store');
Route::get('/admin/file','FileController@view');
Route::get('/admin/file/{id}/delete','FileController@delete');


//Admin profile
    Route::get('admin/adminprofile',function(){
        return view('admin.adminprofile.edit');
    });
    Route::post('admin/adminprofile/update','AdminProfileController@update');

});