<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable=['name','email','phone','gender','address','title','contents','status'];


}
