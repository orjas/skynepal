<?php

namespace App\Providers;

use App\About;
use App\Model\Blog;
use App\Model\Donote;
use App\News;
use App\Setting;
use App\Slider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        date_default_timezone_set('Asia/kathmandu');
        $date=date('Y-m-d');
        View::share('abouts', About::all());
        View::share('setting', Setting::first());
        View::share('sliders',Slider::all());
        View::share('samachar',News::where('category','News')->limit(2)->orderBy('date','desc')->get());
        View::share('blogger',Blog::where('status',1)->limit(2)->orderBy('created_at','desc')->get());
        View::share('upcomeevents',News::whereDate('date','>',$date)->where('category','Event')->orderBy('date','desc')->limit(2)->get());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
