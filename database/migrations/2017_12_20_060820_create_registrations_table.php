<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone');
            $table->integer('age');
            $table->string('gender');
            $table->string('address');
            $table->string('education')->nullable();
            $table->string('profession')->nullable();;
            $table->string('image')->nullable();
            $table->string('physical')->nullable();
            $table->string('recent_course')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrations');
    }
}
