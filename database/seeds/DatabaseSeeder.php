<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=\Faker\Factory::create();
        Model::unguard();
        \App\User::create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('password'),

        ]);
        \App\About::create([
            'title'=>$faker->name,
            'category'=>'our',
            'description'=>$faker->paragraph(),
            'contact'=>1,
            'registration'=>1,

        ]);
        \App\Slider::create([
            'image'=>$faker->name,
            'title'=>$faker->name,
            'subtitle'=>$faker->name,
        ]);
        \App\Setting::create([
            'title'=>$faker->name,
            'subtitle'=>$faker->name,
            'description'=>$faker->paragraph,
            'message'=>$faker->paragraph,
            'email'=>$faker->name,
            'phone'=>$faker->phoneNumber,
            'address'=>$faker->address,
            'logo'=>$faker->name,
            'facebook'=>$faker->name,
        ]);
        \App\News::create([

        ]);

        // $this->call(UserTableSeeder::class);

        Model::reguard();
    }
}
