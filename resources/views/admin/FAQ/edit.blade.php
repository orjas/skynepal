@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/faq')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    {!! Form::open(['url' => 'admin/faq/'.$question->id.'/update', 'method' => 'post','files'=>true]) !!}
    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Title</label>
        <input type="text" class="form-control" name="title" value="{{$question->title}}">
        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
    </div>






    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Description</label>
        <textarea class="form-control simpleEditor" name="description">{!! $question->description !!}</textarea>
    </div>
    @if ($errors->has('description'))
        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
    @endif


    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
    {!! Form::close() !!}


@endsection
{!! Form::hidden('name', '', ['id' => 'id']) !!}



