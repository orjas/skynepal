@extends('admin.layouts.main')


@section('content')

    <div class="col-md-1">
        <a href="{{url('admin/faq/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>

                    <th>Description</th>

                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($questions as $question)
                    <tr>
                        <td>{{$question->title}}</td>
                        <td>{!!  $question->description!!}</td>

                        <td>
                            <a href={{url('/admin/faq/'.$question->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td><td>
                            <form method="GET" action={{url('admin/faq/'.$question->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>

                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $questions->render()!!}
        </div>
    </div>

@section('script')
    <script>
        setTimeout(function() {
            $('.Mesaagae').fadeOut('fast');
        }, 30000);
    </script>
@endsection
@endsection






