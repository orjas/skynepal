@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/about')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <br><br>
    {!! Form::open(['url' => 'admin/about/add', 'method' => 'post','files'=>true]) !!}



    <br>
    <div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">
        <label >Title</label>
       <input class="form-control" name="title">
        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group  {{ $errors->has('category') ? ' has-error' : '' }}">
        <label >Choose one category</label>
        <select name="category" class="form-control">
            <option value="our">हाम्रो</option>
            <option value="opportunity">अवसर</option>
            <option value="participation">सहभागिता</option>
        </select>

    </div>
    <div class="form-group  {{ $errors->has('description') ? ' has-error' : '' }}">
        <label >Description</label>
        <textarea class="form-group " id="editor" name="description"></textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
        @endif
    </div>

    <div class="form-group ">
        <input type="checkbox"  name="contact"><label >Contact</label>
       &nbsp; <input type="checkbox"  name="registration"><label >Registrtion</label>


    </div>



      <div class="text-center">
    {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
      </div>
    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
