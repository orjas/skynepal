@extends('admin.layouts.main')


@section('content')
    <div class="col-md-1">
        <a href="{{url('admin/about/add')}}" ><button class="btn btn-primary btn-lg" >Add</button></a>
    </div>

    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>



                    <th>Title</th>
                    <th>Category</th>

                    <th>Contact</th>
                    <th>Registration</th>

                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                    <tr>

                       @foreach($abouts as $about)


                        <td>{{$about->title}}</td>
                        <td>{{$about->category}}</td>

                        <td>  <input type="checkbox"  {!! $about->contact==1?'checked':"" !!} disabled></td>
                        <td><input type="checkbox"   {!! $about->registration==1?'checked':"" !!} disabled ></td>


                        <td>

                            <a href={{url('/admin/about/'.$about->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>

                        </td><td>
                                <form method="GET" action={{url('admin/about/'.$about->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                    <button type="button" class="btn btn-danger " href="#"
                                            data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                        Delete</button>
                                </form>

                        </td>
                    </tr>
                           @endforeach



                </tbody>
            </table>
{!! $abouts->render() !!}
        </div>
    </div>

@endsection

