@extends('admin.layouts.main')


@section('content')


    {!! Form::open(['url' => 'admin/adminprofile/update', 'method' => 'post']) !!}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label >Name</label>
            <input type="text" class="form-control" name="name" value="{{Auth::user()->name}}">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
        </div>
    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        <label >email</label>
        <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}">
        @if ($errors->has('date'))
            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
        @endif
    </div>
    <p class="alert alert-info">Please Login With new Credential after changing email or password</p>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label>Password</label>
            <input type="password" class="form-control" name="password">
            <span>Password must be of 8 character</span>
            @if ($errors->has('password'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>
     @if(Session::get('confirm'))
         <p class="alert alert-danger">Password Donot Match</p>
         @endif

    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
        <label>Confirm Password</label>
        <input type="password" class="form-control" name="confirm_password">


    </div>

    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection



