@extends('admin.layouts.main')


@section('content')
    <?php $sex_types=Config::get('constants.SEX_TYPES');
    $status=[
            0=>'IN ACTIVE',
            1=>'ACTIVE'
    ]
    ?>

    <div class="col-md-offset-9">
        <a href="{{url('admin/blogs')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>

    {!! Form::model($blogs, ['url' => '#', 'method' => 'post','class'=>'form-horizontal','files'=>true]) !!}


    <fieldset disabled>
            <legend> </legend>
            <div class="row height50">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label>नाम</label>
                        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Your Name']) !!}

                        @if ($errors->has('name'))
                            <span class="help-block">
                                                                            <strong>{{ $errors->first('name') }}</strong>
                                                                        </span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>ईमेल</label>
                        {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Your Email']) !!}
                        @if ($errors->has('email'))
                            <span class="help-block">
                                                                            <strong>{{ $errors->first('email') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label>फोन</label>
                        {!! Form::text('phone', null, ['class' => 'form-control','placeholder'=>'Your Phone Number']) !!}

                        @if ($errors->has('phone'))
                            <span class="help-block">
                                                                            <strong>{{ $errors->first('phone') }}</strong></span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label >लिङ्ग</label>
                        {!! Form::select('gender', $sex_types ,$blogs->gender , ['class' => 'form-control']) !!}
                        @if ($errors->has('gender'))
                            <span class="help-block">
                                                                            <strong>{{ $errors->first('gender') }}</strong>
                                                                    </span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                        <label>ठेगाना</label>
                        {!! Form::text('address', null, ['class' => 'form-control','placeholder'=>'Your Address']) !!}

                        @if ($errors->has('address'))
                            <span class="help-block">
                                                                             <strong>{{ $errors->first('address') }}</strong>
                                                                        </span>
                        @endif
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label>ब्लग शिर्षक</label>
                        {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>'Your Blog Title']) !!}

                        @if ($errors->has('title'))
                            <span class="help-block">
                                                                             <strong>{{ $errors->first('title') }}</strong>
                                                                        </span>
                        @endif
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('contents') ? 'has-error' : '' }}">
                        <label>ब्लग contents</label>
                        {!! Form::textarea('contents', null, ['class' => 'form-control','placeholder'=>'Your blog contents','rows'=>16]) !!}

                        @if ($errors->has('contents'))
                            <span class="help-block"  style="color: red">
                                                                             <strong >{{ $errors->first('contents') }}</strong>
                                                                        </span>
                        @endif
                    </div>
                </div>
            </div>


        </fieldset>
    {!! Form::close() !!}
    <form method="POST" action={{url('admin/blogs')}} accept-charset="UTF-8">
        {!! csrf_field() !!}
        <input name="id" type="hidden" value="{{$blogs->id}}">
        <input name="status" type="hidden" value="{{$blogs->status}}">
        <button type="submit" class="btn {{$blogs->status?'btn-primary':'btn-danger'}}">
            {!! $status[$blogs->status]!!}</button>
    </form>


@endsection


