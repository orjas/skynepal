@extends('admin.layouts.main')


@section('content')
    <?php $sex_types=Config::get('constants.SEX_TYPES');
        $status=[
            0=>'IN ACTIVE',
            1=>'ACTIVE'
        ]
    ?>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Blog Title</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>View</th>
                    <th>Status</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($blogs as $blog)
                    <tr>
                        <td>{{$blog->title}}</td>
                        <td>{{$blog->name}}</td>
                        <td>{!!  $blog->email!!}</td>
                        <td>{!!  $blog->phone!!}</td>
                        <td>{!!  $sex_types[$blog->gender]!!}</td>
                        <td>{!!  $blog->address!!}</td>
                        <td>
                            <a href={{url('admin/blogs/'.$blog->id.'/edit')}}><button class="btn btn-primary" >View</button></a>
                        </td>
                        <td>
                            <form method="POST" action={{url('admin/blogs')}} accept-charset="UTF-8">
                                {!! csrf_field() !!}
                                <input name="id" type="hidden" value="{{$blog->id}}">
                                <input name="status" type="hidden" value="{{$blog->status}}">
                                <button type="submit" class="btn {{$blog->status?'btn-primary':'btn-danger'}}">
                                    {!! $status[$blog->status]!!}</button>
                            </form>
                        </td>

                        <td>
                            <form method="GET" action={{url('admin/blogs/'.$blog->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>
                        </td>
                    </tr>

                @endforeach
                </tbody>
            </table>
            {!!  $blogs->render()!!}
        </div>
    </div>
@endsection
@section('script')
    <script>
        setTimeout(function() {
            $('.Mesaagae').fadeOut('fast');
        }, 30000);

    </script>
@endsection







