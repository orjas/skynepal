@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/category')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>

    <form method="post" action="{{url('admin/category/'.$category->id.'/update')}}" enctype="multipart/form-data">

    <div class="form-group  {{ $errors->has('name') ? ' has-error' : '' }} ">
        <label> Name</label>
        <input type="text" class="form-control" name="name" value="{{$category->name}}">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
            <label >Feature Image</label>
            <img src="{{asset('/images/gallery/'.$category->image)}}" width="120" align="right">
            <br><br><br>
            <input type="file" class="form-control" name="image">
            @if ($errors->has('image'))
                <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
            @endif
        </div>


        <div class="text-center">
            {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
@endsection


