@extends('admin.layouts.main')


@section('content')
<?php $i=1;?>
    <div class="col-md-1">
        <a href="{{url('admin/category/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                     <th>S.N.</th>
                    <th>Category Name</th>
                     <th>Feature Image</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>

                         <td>{{$i}}</td>
                        <?php $i++;?>
                        <td>{{$category->name}}</td>
                        <td><img src="{{asset('images/gallery/'.$category->image)}}" height="100"></td>

                        <td>
                            <a href={{url('admin/category/'.$category->id.'/edit')}}><button class="btn btn-primary" >Edit</button></a>
                        </td>
                        <td>
                            <form method="GET" action={{url('admin/category/'.$category->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>


                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $categories->render()!!}
        </div>
    </div>

@endsection

