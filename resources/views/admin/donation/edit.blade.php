@extends('admin.layouts.main')


@section('content')


    <br><br>
    {!! Form::model($donation,['url' => 'admin/donation', 'method' => 'post']) !!}
    {!! Form::hidden('id', $donation->id, ['id' => 'id']) !!}
    <div class="form-group  {{ $errors->has('description') ? ' has-error' : '' }}">
        <label >Description</label>
        {!! Form::textarea('description', null, ['class' => 'form-control','id'=>'editor']) !!}
        @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>

    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>

    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
