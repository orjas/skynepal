@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/file')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/file/add')}}" enctype="multipart/form-data">
    <div class="form-group  {{ $errors->has('name') ? ' has-error' : '' }} ">
        <label> Name</label>
        <input type="text" class="form-control" name="name">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
        <div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
            <label >Feature File</label>
            <input type="file" class="form-control" name="file">
            @if ($errors->has('file'))
                <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
            @endif
        </div>


        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
@endsection


