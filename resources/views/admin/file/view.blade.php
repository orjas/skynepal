@extends('admin.layouts.main')


@section('content')
<?php $i=1;?>
    <div class="col-md-1">
        <a href="{{url('admin/file/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                     <th>S.N.</th>
                    <th>File name </th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($files as $file)
                    <tr>

                         <td>{{$i}}</td>
                        <?php $i++;
                        ?>

                        <td  ><span class='fa fa-file-{!! $file->extension !!}-o fa-lg'></span> <a href="{{asset('file/'.$file->file)}}" target="_blank">{{$file->name}}</a></td>

                        <td>
                            <form method="GET" action={{url('admin/file/'.$file->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>


                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $files->render()!!}
        </div>
    </div>

@endsection

