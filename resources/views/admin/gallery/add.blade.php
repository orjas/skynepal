@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/gallery')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/gallery/add')}}" enctype="multipart/form-data">
           <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
            <label >Image</label>
            <input type="file" class="form-control" name="image">
               @if ($errors->has('image'))
                   <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
               @endif
        </div>
    <div class="form-group ">
        <label> Select a Category</label>


        {!! Form::select('category', $categories, null, ['class'=>'form-control']) !!}
    </div>

        <div class="form-group ">
            <label> Description</label>
            {!! Form::text('description', null, ['class' => 'form-control']) !!}
        </div>

        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
@endsection


