@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/gallery')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <br><br>
    {!! Form::open(['url' => 'admin/gallery/'.$gallerys->id.'/update', 'method' => 'post','files'=>true]) !!}


        <div class="form-group">
            <label >Image</label>

            <img src="{{asset('/images/gallery/'.$gallerys->image)}}" width="120" align="right">


            <input type="file"  name="image">
          </div>
        <br><br><br><br>
    <div class="form-group ">
        <label> Select a Category</label>
        {!! Form::select('category', $categories, $gallerys->category, ['class'=>'form-control']) !!}

    </div>

    <div class="form-group ">
        <label> Description</label>
        {!! Form::text('description', $gallerys->description, ['class' => 'form-control']) !!}
    </div>

    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
