@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/homepost')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/homepost/add')}}" enctype="multipart/form-data">
        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" class="form-control" name="title">
            @if ($errors->has('title'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group">
            <label>Select a Category</label>
            <select class="form-control" name="category">
                <option value="Top_post">Top_post</option>
                <option value="Bottom_post">Bottom_post</option>
            </select>
        </div>
        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
            <label >Image</label>
            <input type="file" class="form-control" name="image">
            @if ($errors->has('image'))
                <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
            @endif
        </div>



    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        <label>Description</label>
        <textarea name="description" class="form-control" id="editor"></textarea>

        @if ($errors->has('description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
        @endif
    </div>
        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
@endsection

