@extends('admin.layouts.main')

@section('content')
    <div class="col-md-offset-9">
        <a href="{{url('admin/homepost')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>

    {!! Form::open(['url' => 'admin/homepost/'.$homepost->id.'/update', 'method' => 'post','files'=>true]) !!}

    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label >Title</label>
            <input type="text" class="form-control" name="title" value="{{$homepost->title}}">
        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
        </div>
    <div class="form-group">
        <label>Select a Category</label>
        <select class="form-control" name="category">
            <option value="{{$homepost->category}}">{{$homepost->category}}</option>
            <option value="Top_post">Top_post</option>
            <option value="Bottom_post">Bottom_post</option>

        </select>
    </div>
        <div class="form-group">
            <label >Image</label>

            <img src="{{asset('/images/homepost/'.$homepost->image)}}" width="150" align="right">


            <input type="file"  name="image">
          </div>
        <br><br>

        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label>Description</label>
            <textarea name="description" class="form-control" id="editor">{!! $homepost->description !!}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
            @endif
        </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection



