@extends('admin.layouts.main')


@section('content')

    <div class="col-sm-3">
        <a href="{{url('admin/homepost/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                     <th>Category</th>

                    <th>Image</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($homeposts as $homepost)
                    <tr>
                        <td>{{$homepost->title}}</td>
                         <td>{{$homepost->category}}</td>

                        <td><img src="{{asset('images/homepost/'.$homepost->image)}}" height="100"></td>

                        <td>
                        <a href={{url('/admin/homepost/'.$homepost->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>

                        <td>
                            <form method="GET" action={{url('admin/homepost/'.$homepost->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $homeposts->render()!!}
        </div>
    </div>

@endsection


