<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
            <li class="{!! Request::segment(1)=='dashboard'?'active':'' !!}"><a href="{{url("/admin/dashboard")}}"><i class="fa fa-edit "></i> Dashboard<span class=""></span></a></li>

            <li><a><i class="fa fa-edit"></i> Home<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">

                    <li class="{!! Request::segment(2)=='slider'?'active':'' !!}"><a href="{{url("/admin/slider")}}"><i class="fa fa-edit "></i> Slider<span class=""></span></a></li>
                    <li class="{!! Request::segment(2)=='testomonial'?'active':'' !!}"><a href="{{url("/admin/testomonial")}}"><i class="fa fa-edit "></i> Testomonial<span class=""></span></a></li>
                    <li class="{!! Request::segment(2)=='homepost'?'active':'' !!}"><a href="{{url("/admin/homepost")}}"><i class="fa fa-edit "></i> Home Post<span class=""></span></a></li>



                </ul>
            </li>
            <li class="{!! Request::segment(2)=='about'?'active':'' !!}"><a href="{{url("/admin/about")}}"><i class="fa fa-edit "></i> About<span class=""></span></a></li>
            <li class="{!! Request::segment(2)=='blogs'?'active':'' !!}"><a href="{{url("/admin/blogs")}}"><i class="fa fa-edit "></i> Blogs<span class=""></span></a></li>
            <li><a><i class="fa fa-edit"></i> Gallery<span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="{!! Request::segment(2)=='gallery'?'active':'' !!}"><a href="{{url("/admin/gallery")}}">Add Image</a></li>
                    <li class="{!! Request::segment(2)=='category'?'active':'' !!}"><a href="{{url("/admin/category")}}">Add Image Category</a></li>


                </ul>
            </li>
            <li class="{!! Request::segment(2)=='team'?'active':'' !!}"><a href="{{url("/admin/team")}}"><i class="fa fa-edit "></i> Team<span class=""></span></a></li>
            <li class="{!! Request::segment(2)=='message'?'active':'' !!}"><a href="{{url("/admin/message")}}"><i class="fa fa-edit "></i> Message<span class=""></span></a></li>
            <li class="{!! Request::segment(2)=='registration'?'active':'' !!}"><a href="{{url("/admin/registration")}}"><i class="fa fa-edit "></i> Registration<span class=""></span></a></li>
            <li class="{!! Request::segment(2)=='video'?'active':'' !!}"><a href="{{url("/admin/video")}}"><i class="fa fa-edit "></i> Video<span class=""></span></a></li>
            <li class="{!! Request::segment(2)=='faq'?'active':'' !!}"><a href="{{url("/admin/faq")}}"><i class="fa fa-edit "></i> FAQ<span class=""></span></a></li>
            <li class="{!! Request::segment(2)=='news'?'active':'' !!}"><a href="{{url("/admin/news")}}"><i class="fa fa-edit "></i> News<span class=""></span></a></li>
            <li class="{!! Request::segment(2)=='file'?'active':'' !!}"><a href="{{url("/admin/file")}}"><i class="fa fa-edit "></i> File<span class=""></span></a></li>


            <li class="{!! Request::segment(2)=='donation'?'active':'' !!}"><a href="{{url("/admin/donation")}}"><i class="fa fa-edit "></i> Donation<span class=""></span></a></li>

            <li class="{!! Request::segment(2)=='setting'?'active':'' !!}"><a href="{{url("/admin/setting")}}"><i class="fa fa-edit "></i> Setting <span class=""></span></a></li>


        </ul>
    </div>


</div>