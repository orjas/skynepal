<?php $url=\Illuminate\Support\Facades\Config::get('constants.SITE_URL');?>
<!-- jQuery -->
<script src="{{asset('admin/js/jquery.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>

<!-- FastClick -->
<script src="{{asset('admin/js/fastclick.js')}}"></script>

<!-- NProgress -->
<script src="{{asset('admin/js/nprogress.js')}}"></script>

<!-- jQuery custom content scroller -->
<script src="{{asset('admin/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>

<!-- Custom Theme Scripts -->

<script src="{{asset('admin/js/custom.min.js')}}"></script>
<script src="{{asset('admin/js/notify.js')}}"></script>
<script src={{ asset('tinymce/js/tinymce/tinymce.min.js')}}></script>
<script src={{ asset('ckeditor/ckeditor.js')}}></script>

<script>

    tinymce.init({


        selector: ".edit",theme: "modern",width: 680,height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
        toolbar2: "|responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
        image_advtab: true ,
        relative_urls:false,

        external_filemanager_path:"{{$url}}/assest/tinymce/js/tinymce/plugins/responsivefilemanager/",
        filemanager_title:"filemanager" ,
        external_plugins: { "filemanager" :"{{$url}}/assest/tinymce/js/tinymce/plugins/responsivefilemanager/plugin.min.js"}
    });
    tinymce.init({
        selector: '.simpleEditor',  // change this value according to your HTML
        menu: {
            view: {title: 'Edit', items: 'cut, copy, paste'}
        }
    });

</script>
<script>
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace( 'editor1' );
</script>

<script>
    CKEDITOR.replace( 'editor' ,{
        filebrowserBrowseUrl : '{{$url}}/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserUploadUrl : '{{$url}}/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
        filebrowserImageBrowseUrl : '{{$url}}/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
    });
</script>