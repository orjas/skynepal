
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Simplified Kundalini Yoga | SKY Nepal</title>
    <link rel="shortcut icon" href="{{asset("favicon.ico")}}" type="image/x-icon">
    <link rel="icon" href="{{asset("favicon.ico")}}" type="image/x-icon">
   @include('admin.includes.favicon')
 @include('admin.layouts.addstyle')
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body class="nav-md">
<div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Parmanently</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure about this ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" id="confirm">Delete</button>
            </div>
        </div>
    </div>
</div>

{{-----------------------center of page----------------------}}
<style>
    body.modal-open .modal {
        display: flex !important;
        align-items: center;
    }
</style>
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

               @include('admin.includes.topsidebar')

                <!-- /menu profile quick info -->
                   <br />

                <!-- sidebar menu -->
               @include('admin.includes.sidebarmenu')
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->

                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        @include('admin.includes.topnavigation')
        <!-- /top navigation -->
    <?php $message='';?>
    <?php $message= Session::get('alert-success');?>
        <!-- page content -->
        <div class="right_col" role="main">


                    <div class="title_left">
                        <div class="x_panel ">
                        @yield('content')
                            </div>
                    </div>


        </div>
        <!-- /page content -->

        <!-- footer content -->
      @include('admin.includes.footer')
        <!-- /footer content -->
    </div>
</div>

@include('admin.layouts.addscripts')
<script>
    var success="{{$message}}";
    if(success!=='') {
        $.notify(
                success,
                { className:'success',
                    globalPosition: 'top center'}
        );
    }


</script>
<script>
    $('#confirmDelete').on('show.bs.modal', function (e) {

        // Pass form reference to modal for submission on yes/ok
        var form = $(e.relatedTarget).closest('form');
        $(this).find('.modal-footer #confirm').data('form', form);
    });
    <!-- Form confirm (yes/ok) handler, submits form -->
    $('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
        $(this).data('form').submit();
    });

</script>
</body>
</html>