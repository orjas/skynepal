@extends('admin.layouts.main')



@section('content')
    <div class="col-md-9"><div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Delivered Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($messages as $message)
                    <tr>
                        <td>{{$message->name}}</td>
                        <td>{{$message->email}}</td>
                        <td>{{$message->message}}</td>
                        <td>{{$message->created_at}}</td>
                        <td>

                            <form method="GET" action={{url('admin/message/'.$message->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>

                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $messages->render()!!}
        </div>
    </div>

@endsection


