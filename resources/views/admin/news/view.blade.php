@extends('admin.layouts.main')


@section('content')

    <div class="col-sm-3">
        <a href="{{url('admin/news/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                     <th>Category</th>
                    <th>Date</th>
                    <th>Short Description</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($newses as $news)
                    <tr>
                        <td>{{$news->title}}</td>
                         <td>{{$news->category}}</td>
                         <td>{{$news->date}}</td>
                        <td>{!!  $news->short_description!!}</td>
                        <td>
                            <a href={{url('/admin/news/'.$news->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>

                        <td>
                            <form method="GET" action={{url('admin/news/'.$news->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $newses->render()!!}
        </div>
    </div>

@endsection


