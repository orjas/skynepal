@extends('admin.layouts.main')



@section('content')
    <div class="col-md-9"><div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Course</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Address</th>
                    <th>Education</th>
                    <th>Profession</th>
                    <th>Physical</th>
                    <th>Recent Course</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($registrations as $registration)
                    <tr>


                        <?php $courses=json_decode($registration->course);

                        ?>


                        <td><img src="{{asset('/images/registration/'.$registration->image)}}"  alt="No image" width="60"></td>
                        <td>
                            <ul>
                                @forelse($courses as $course)
                                <li>{{$course}}</li>
                                @empty
                                    <li>No Selection</li>
                                @endforelse
                            </ul>
                        </td>
                        <td>{{$registration->name}}</td>
                        <td>{{$registration->email}}</td>
                        <td>{{$registration->phone}}</td>
                        <td>{{$registration->gender}}</td>
                        <td>{{$registration->age}}</td>
                        <td>{{$registration->address}}</td>
                        <td>{{$registration->education}}</td>
                        <td>{{$registration->profession}}</td>
                        <td>{{$registration->physical}}</td>
                        <td>{{$registration->recent_course}}</td>
                        <td>
                            <form method="GET" action={{url('admin/registration/'.$registration->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>

                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $registrations->render()!!}
        </div>
    </div>

@endsection


