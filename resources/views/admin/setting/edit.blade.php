@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/setting')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <br><br>
    {!! Form::open(['url' => 'admin/setting/'.$setting->id.'/update', 'method' => 'post','files'=>true]) !!}


    <div class="form-group  {{ $errors->has('logo') ? ' has-error' : '' }}">
        <label >Logo</label>

        <img src="{{asset('images/logo/'.$setting->logo)}}" width="120" align="right">


        <input type="file"  name="logo">
        @if ($errors->has('logo'))
            <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
        @endif
    </div>
    <br><br><br><br>
    <div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Title</label>
        <input type="text" name="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$setting->title}}">
        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
    </div>
    <br>
    <div class="form-group  {{ $errors->has('subtitle') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Subtitle</label>
        <input type="text" name='subtitle' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$setting->subtitle}}" >
        @if ($errors->has('subtitle'))
            <span class="help-block">
                                        <strong>{{ $errors->first('subtitle') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group  {{ $errors->has('description') ? ' has-error' : '' }}">
        <label >Description</label>
        <textarea class="form-group " id="editor" name="description">{!! $setting->description !!}</textarea>
        @if ($errors->has('description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group  {{ $errors->has('message') ? ' has-error' : '' }}">
        <label >Message</label>
        <textarea class="form-group simpleEditor"  name="message">{!! $setting->description !!}</textarea>
        @if ($errors->has('message'))
            <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group  {{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" name='email' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$setting->email}}" >
        @if ($errors->has('email'))
            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group  {{ $errors->has('phone') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Phone</label>
        <input type="text" name='phone' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$setting->phone}}" >
        @if ($errors->has('phone'))
            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group  {{ $errors->has('address') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Addrsss</label>
        <input type="text" name='address' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$setting->address}}" >
        @if ($errors->has('address'))
            <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group  {{ $errors->has('facebook') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Facebook</label>
        <input type="text" name='facebook' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$setting->facebook}}" >
        @if ($errors->has('facebook'))
            <span class="help-block">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
