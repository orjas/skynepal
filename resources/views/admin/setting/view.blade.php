@extends('admin.layouts.main')


@section('content')


    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>


                    <th>Logo</th>
                    <th>Title</th>
                    <th>Sub Title</th>

                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Facebook</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <tr>


                        <td><img src="{{asset('images/logo/'.$setting->logo)}}" height="100"></td>
                        <td>{{$setting->title}}</td>
                        <td>{{$setting->subtitle}}</td>
                     
                        <td>{{$setting->email}}</td>
                        <td>{{$setting->phone}}</td>
                        <td>{{$setting->address}}</td>
                        <td>{{$setting->facebook}}</td>

                        <td>
                            <a href={{url('/admin/setting/'.$setting->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>
                    </tr>


                </tbody>
            </table>

        </div>
    </div>

@endsection

