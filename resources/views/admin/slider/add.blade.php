@extends('admin.layouts.main')
@section('content')
   <div class="">
                    <a href="{{url('admin/slider')}}" ><button class="btn btn-primary " >Back</button></a>
                </div>
   <br/>
                {!! Form::open(['url' => 'admin/slider/add', 'method' => 'post','files'=>'true']) !!}
   <div class="form-group  {{ $errors->has('image') ? ' has-error' : '' }}">
       <label >Image</label>




       <input type="file"  name="image">
       @if ($errors->has('image'))
           <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
       @endif
   </div>


   <br>
                    <div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" name="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                        @if ($errors->has('title'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>
   <br>
   <div class="form-group  {{ $errors->has('subtitle') ? ' has-error' : '' }}">
       <label for="exampleInputEmail1">Subtitle</label>
       <input type="text" name='subtitle' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
       @if ($errors->has('subtitle'))
           <span class="help-block">
                                        <strong>{{ $errors->first('subtitle') }}</strong>
                                    </span>
       @endif
   </div>
   <div class="text-center">
       {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
   </div>

   {!! Form::close() !!}





@endsection