@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/slider')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <br><br>
    {!! Form::open(['url' => 'admin/slider/'.$slider->id.'/update', 'method' => 'post','files'=>true]) !!}


    <div class="form-group  {{ $errors->has('image') ? ' has-error' : '' }}">
        <label >Image</label>

        <img src="{{asset('images/slider/'.$slider->image)}}" height="120" align="right">


        <input type="file"  name="image">
        @if ($errors->has('image'))
            <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
        @endif
    </div>
    <br><br><br><br>
    <div class="form-group  {{ $errors->has('title') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Title</label>
        <input type="text" name="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$slider->title}}">
        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
    </div>
    <br>
    <div class="form-group  {{ $errors->has('subtitle') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Subtitle</label>
        <input type="text" name='subtitle' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$slider->subtitle}}" >
        @if ($errors->has('subtitle'))
            <span class="help-block">
                                        <strong>{{ $errors->first('subtitle') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>

    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
