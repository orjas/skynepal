@extends('admin.layouts.main')


@section('content')

    <div class="col-md-1">
        <a href="{{url('admin/slider/add')}}" ><button class="btn btn-primary " >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>


                    <th>Image</th>
                    <th>Title</th>
                    <th>Sub Title</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($sliders as $slider)

                    <tr>


                        <td><img src="{{asset('images/slider/'.$slider->image)}}" height="100"></td>
                        <td>{{$slider->title}}</td>
                        <td>{{$slider->subtitle}}</td>
                        <td>
                            <a href={{url('/admin/slider/'.$slider->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>
                        <td>
                            <form method="GET" action={{url('admin/slider/'.$slider->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>
                        </td>

                    </tr>

                @empty
                    <tr>
                    <td><Strong>No Data Found</Strong></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!!  $sliders->render()!!}
        </div>
    </div>

@endsection

