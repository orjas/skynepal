@extends('admin.layouts.main')
@section('content')
   <div class="">
                    <a href="{{url('/admin/team')}}" ><button class="btn btn-primary " >Back</button></a>
                </div>
   <br/>
                {!! Form::open(['url' => 'admin/team/add', 'method' => 'post','files'=>'true']) !!}
   <div class="form-group  {{ $errors->has('image') ? ' has-error' : '' }}">
       <label >Image</label>




       <input type="file"  name="image">
       @if ($errors->has('image'))
           <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
       @endif
   </div>


   <br>
                    <div class="form-group  {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
   <br>
   <div class="form-group  {{ $errors->has('position') ? ' has-error' : '' }}">
       <label for="exampleInputEmail1">Position</label>
       <input type="text" name='position' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
       @if ($errors->has('position'))
           <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
       @endif
   </div>
   <div class="text-center">
       {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
   </div>

   {!! Form::close() !!}





@endsection