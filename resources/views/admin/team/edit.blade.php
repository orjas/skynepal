@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/team')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <br><br>
    {!! Form::open(['url' => 'admin/team/'.$team->id.'/update', 'method' => 'post','files'=>true]) !!}


    <div class="form-group  {{ $errors->has('image') ? ' has-error' : '' }}">
        <label >Image</label>

        <img src="{{asset('images/team/'.$team->image)}}" width="120" align="right">


        <input type="file"  name="image">
        @if ($errors->has('image'))
            <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
        @endif
    </div>
    <br><br><br><br>
    <div class="form-group  {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Name</label>
        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$team->name}}">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
    <br>
    <div class="form-group  {{ $errors->has('position') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Position</label>
        <input type="text" name='position' class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$team->position}}" >
        @if ($errors->has('position'))
            <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
