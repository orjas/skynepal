@extends('admin.layouts.main')


@section('content')

    <div class="col-md-1">
        <a href="{{url('admin/team/add')}}" ><button class="btn btn-primary " >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>


                    <th>Image</th>
                    <th>Name</th>
                    <th>Position</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($teams as $team)

                    <tr>


                        <td><img src="{{asset('images/team/'.$team->image)}}" height="100"></td>
                        <td>{{$team->name}}</td>
                        <td>{{$team->position}}</td>
                        <td>
                        <a href={{url('/admin/team/'.$team->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>
                        <td>
                            <form method="GET" action={{url('admin/team/'.$team->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>
                        </td>

                    </tr>

                @empty
                    <tr>
                    <td><Strong>No Data Found</Strong></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!!  $teams->render()!!}
        </div>
    </div>

@endsection

