@extends('admin.layouts.main')

@section('content')
    <div class="col-md-offset-9">
        <a href="{{url('admin/testomonial')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    {!! Form::open(['url' => 'admin/testomonial/add', 'method' => 'post','files'=>true]) !!}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Name</label>
        <input type="text" class="form-control" name="name">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('position') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Position</label>
        <input type="text" class="form-control" name="position">
        @if ($errors->has('position'))
            <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group ">
        <label for="exampleInputEmail1">Category</label>
        <select class="form-control" name="category">
            <option value="Testomonial">Testomonial</option>
            {{--<option value="Speech">Speech</option>--}}
        </select>

    </div>
    <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Image</label>
        <input type="file" class="form-control" name="image">
        @if ($errors->has('image'))
            <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
        @endif
    </div>


    <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Message</label>
        <textarea class="form-control simpleEditor" name="message"></textarea>

    @if ($errors->has('message'))
        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
    @endif
    </div>


    <div class="text-center">
        {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
    {!! Form::close() !!}




@endsection


