@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/video')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/video/add')}}" enctype="multipart/form-data">
        <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Category Name</label>
            <input type="text" class="form-control" name="category">
            @if ($errors->has('category'))
                <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
            @endif
        </div>

    <div class="form-group {{ $errors->has('video') ? ' has-error' : '' }}">
        <label>Video URL</label>
        <input type="text" class="form-control" name="video">
        @if ($errors->has('video'))
            <span class="help-block">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
        @endif
    </div>
        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
        </div>>
        {!! csrf_field() !!}
    </form>
@endsection
