@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/video')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    {!! Form::open(['url' => 'admin/video/'.$videos->id.'/update', 'method' => 'post']) !!}

    <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }}">
            <label >Category</label>
            <input type="text" class="form-control" name="category" value="{{$videos->category}}">
        @if ($errors->has('category'))
            <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('video') ? ' has-error' : '' }}">
        <label >Video URL</label>
        <input type="text" class="form-control" name="video" value="{{$videos->url}}">
        @if ($errors->has('video'))
            <span class="help-block">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
        @endif

    </div>


    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection




