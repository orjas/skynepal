@extends('admin.layouts.main')


@section('content')

    <div class="col-md-1">
        <a href="{{url('admin/video/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Category Name</th>
                    <th>Video URL</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($videos as $video)
                    <tr>
                        <td>{{$video->category}}</td>
                        <td>
                            <iframe width="365" height="100%" src="https://www.youtube.com/embed/{{$video->video}}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                           </td>
                       <td>


                        <a href={{url('/admin/video/'.$video->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>
                        <td>
                            <form method="GET" action={{url('admin/video/'.$video->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $videos->render()!!}
        </div>
    </div>

@endsection

