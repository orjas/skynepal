@extends('layouts.main')
@section('content')

    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">

                            <div class="media">
                                <p>  <a href="{{route('events')}}"> All</a> | <a href="{{route('upcoming')}}"class="active" style="color: black"> Upcoming</a>  </p>

                                @foreach($events as $event)
                                    <div class="col-md-12 mt-5">
                                        <?php  list($year,$month,$day)=explode("-", $event->date);

                                        $monthName = date('M',strtotime($event->date));

                                        ?>

                                        <div class="panel panel-default event">

                                            <div class="rsvp col-xs-2 col-sm-2">

                                                <i>{{$day}}</i>
                                                <i>{{$monthName}}</i>
                                                <p>{{$year}}</p>

                                            </div>

                                            <div class="info col-xs-8 col-sm-7">
                                                <br>
                                                {{$event->title}}
                                                <div class="visible-xs"><h3>{!! $event->short_description !!}</h3><h6>{!! $event->description !!}</h6></div>
                                                <div class="hidden-xs">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#location{!! $event->id !!}" aria-controls="location" role="tab" data-toggle="tab">Location</a></li>
                                                        <li role="presentation"><a href="#profile{!! $event->id !!}" aria-controls="profile" role="tab" data-toggle="tab">Detail</a></li>
                                                    </ul>
                                                    <!-- Tab panes -->
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="location{!! $event->id !!}">{!!  $event->short_description!!}</div>
                                                        <div role="tabpanel" class="tab-pane" id="profile{!! $event->id !!}">
                                                            {!! $event->description !!}
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>

                                        </div>
                                        <br><br><br><br>

                                    </div>
                                @endforeach


                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection