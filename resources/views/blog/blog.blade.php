@extends('layouts.main')
@section('content')
    <?php
    $sex_types=\Illuminate\Support\Facades\Config::get('constants.SEX_TYPES');
    ;?>
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="media">
                                <div class="media-body"><p></p>

                                </div>
                                <div id="innerpage">
                                    <div class="container">
                                        <div class="in-contain-wrap">
                                            <div class="row">
                                                <div class="col-md-8 wow bounceInLeft animated">
                                                    <article>
                                                        <div class="main-title2">
                                                            <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> ब्लग </span>फारम</h3>
                                                            <span><br>यता भरेर submit गरेको ब्लग पोस्ट verification पछि मात्र websiteमा राखिनेछ |
                                                                <br> पोस्ट Englishमा अथवा Unicode मार्फत नेपालीमा भर्नुहोस् | <br>
                                                            </span>
                                                        </div>
                                                        {!! Form::open(['url' => 'blog', 'method' => 'post','files'=>true]) !!}

                                                        <fieldset>
                                                            <legend> </legend>
                                                            <div class="row height50">
                                                                <div class="col-md-12">
                                                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                                        <label>नाम</label>
                                                                        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Your Name']) !!}

                                                                        @if ($errors->has('name'))
                                                                            <span class="help-block">
                                                                            <strong>{{ $errors->first('name') }}</strong>
                                                                        </span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>ईमेल</label>
                                                                        {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Your Email']) !!}
                                                                        @if ($errors->has('email'))
                                                                            <span class="help-block">
                                                                            <strong>{{ $errors->first('email') }}</strong></span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                                        <label>फोन</label>
                                                                        {!! Form::text('phone', null, ['class' => 'form-control','placeholder'=>'Your Phone Number']) !!}

                                                                        @if ($errors->has('phone'))
                                                                            <span class="help-block">
                                                                            <strong>{{ $errors->first('phone') }}</strong></span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label >लिङ्ग</label>
                                                                        {!! Form::select('gender', $sex_types , null , ['class' => 'form-control']) !!}
                                                                        @if ($errors->has('gender'))
                                                                            <span class="help-block">
                                                                            <strong>{{ $errors->first('gender') }}</strong>
                                                                    </span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                                                        <label>ठेगाना</label>
                                                                        {!! Form::text('address', null, ['class' => 'form-control','placeholder'=>'Your Address']) !!}

                                                                        @if ($errors->has('address'))
                                                                            <span class="help-block">
                                                                             <strong>{{ $errors->first('address') }}</strong>
                                                                        </span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                                                        <label>ब्लग शिर्षक</label>
                                                                        {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>'Your Blog Title']) !!}

                                                                        @if ($errors->has('title'))
                                                                            <span class="help-block">
                                                                             <strong>{{ $errors->first('title') }}</strong>
                                                                        </span>
                                                                        @endif
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-12">
                                                                    <div class="form-group {{ $errors->has('contents') ? 'has-error' : '' }}">
                                                                        <label>ब्लग contents</label>
                                                                        {!! Form::textarea('contents', null, ['class' => 'form-control','placeholder'=>'Your blog contents','rows'=>6]) !!}

                                                                        @if ($errors->has('contents'))
                                                                            <span class="help-block"  style="color: red">
                                                                             <strong >{{ $errors->first('contents') }}</strong>
                                                                        </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                {{--<div id="Recaptcha2"></div>--}}
                                                                {{--{!! NoCaptcha::renderJs() !!}--}}

                                                                {!! app('captcha')->display() !!}
                                                                {{--<div class="g-recaptcha" data-sitekey="{{(config('googlekey.key'))}}"></div>--}}

                                                                <div>
                                                                    @if ($errors->has('g-recaptcha-response'))

                                                                        <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>

                                                                    @endif

                                                                </div>


                                                                <button type="submit" class="btn btn-success pull-right" id="btnContactUs">
                                                                    Submit</button>
                                                            </div>

                                                        </fieldset>
                                                        {{--<div class="text-right">--}}
                                                        {{--<button class="btn btn-send" type="submit">Send <i class="fa fa-paper-plane-o"></i> </button>--}}
                                                        {{--</div>--}}
                                                        {!! Form::close() !!}
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--<script type="text/javascript">--}}
    {{--var CaptchaCallback = function() {--}}
    {{--grecaptcha.render('Recaptcha2', {'sitekey' : '6Lc92z4UAAAAABjNc4S1OhXFFfFMaXlrkXSIHhf4'});--}}
    {{--//            grecaptcha.render('RecaptchaField2', {'sitekey' : '6Lc_your_site_key'});--}}
    {{--};--}}
    {{--</script>--}}

@endsection