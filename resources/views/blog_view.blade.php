@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> </span>{{$blog->title}}</h3>
                            </div>
                            <div class="media">
                                <div class="media-left hidden-small"> </div>
                                <div class="media-body" style="text-align: justify">
                                    <p >{!! $blog->contents!!}</p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div>
                        <div class="col-md-4">
                                <a href="{{route('registration')}}" >  <button class="pull-right btn btn-lg btn-success">आजै दर्ता गर्नुहोस् ।</button></a></div>
                        <div class="col-md-6">
                                <a href="{{route('contact')}}" >  <button class="pull-right btn btn-lg btn-success">सम्पर्क गर्नुहोस</button></a>
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>





@endsection