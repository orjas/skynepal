@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> </span>{{$about->title}}</h3>
                            </div>
                            <div class="media">
                                <div class="media-left hidden-small"> </div>
                                <div class="media-body" style="text-align: justify">
                                    <p >{!! $about->description!!}</p>
                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>
                    <div>
                        <div class="col-md-4">
                            @if($about->registration==1)
                                <a href="{{route('registration')}}" >  <button class="pull-right btn btn-lg btn-success">आजै दर्ता गर्नुहोस् ।</button></a></div>
                        @endif
                        <div class="col-md-6">
                            @if($about->contact==1)
                                <a href="{{route('contact')}}" >  <button class="pull-right btn btn-lg btn-success">सम्पर्क गर्नुहोस</button></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>


        </div>

    </div>





@endsection