@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text" aria-hidden="true"></i> </span> ब्लग</h3>
                                <br>
                                {!! $setting->message!!}
                            </div>
                            <div class="media">
                                <div class="media-left hidden-small"> </div>
                                <div class="media-body">
                                    @forelse($blogs as $blog  )
                                        <div class="col-md-5">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <p style="font-weight: bold;">{{$blog->title}}</p>
                                                    <span class="fa fa-clock-o"> &nbsp;{{$blog->created_at}}</span>
                                                    <p><a href="{{url('blog/'.$blog->id)}}">Read more</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="col-md-5">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <p style="font-weight: bold;">ब्लगहरु उपलब्ध छैन</p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection