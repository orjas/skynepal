@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> </span> समाचार</h3>
                                <br>
                                {!! $setting->message!!}
                            </div>
                            <div class="media">
                                <div class="media-left hidden-small"> </div>
                                <div class="media-body">
                                    @foreach($news as $new  )

                                    <div class="col-md-5">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <p style="font-weight: bold;">{{$new->title}}</p>
                                                <span class="fa fa-clock-o"> &nbsp;{{$new->date}}</span>

                                                <p>{!! $new->short_description !!}</p>
                                                <p><a href="{{route('samachar',['id'=>$new->id])}}">Read more</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection