@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> </span>{{$about->title}}</h3>
                            </div>
                            <div class="media">
                                <div class="media-left hidden-small"> </div>
                                <div class="media-body">
                                    <p class=>{!!  $about->description!!}</p>
                                </div>

                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>

                </div>
            </div>

        </div>

    </div>


    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInRight animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> Story</span> Green Apple</h3>
                            </div>
                            <div class="media">
                                <div class="pull-right media-right hidden-small"><iframe width="500" height="180" src="https://www.youtube.com/embed/ypB0w2pSxJY" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe> </div>
                                <div class="media-body">
                                    <p>योग ज्ञानको सन्दर्भमा नेपाल एउटा समृद्ध देश हो । विगतका ऋषि(मुनि देखि लिएर आधुनिक युगका ज्ञानिजनले यो क्षेत्रमा मनग्गे योगदान गरेका छन् । फलस्वरुप, नेपालमा धेरै योग केन्द्रहरु सकृय रहेका पाईन्छन् । विभिन्न योग केन्द्रका आ(आफ्नै अवधारणा र अभ्यास प्रक्रिया हुन्छ् ।
                                        कुन्डलिनि शक्तिमा आधारित योग विद्या धेरै पुरानो हो । यसको अभ्यास प्रक्रिया जटिल भएकोले यो लोपोन्मुख भएको हो । सर्व(साधारणले यस ज्ञानको अभ्यास र उपभोग सजिलैसंंग गर्न सकुन भन्ने हेतुले भारतको तामिलनाडु राज्यका वेदाद्रि महाऋिषिले व्यापक अनुसंधान गरि यस विद्यालाई सरल पार्नु भयो र सरलिकृत कुन्डलिनि योगको नामले यसलाई सन १९५८ मा प्रचलनमा ल्याउनु भयो । अहिले विश्वको देशहरुमा यो योग प्रचलनमा छ ।
                                        नेपालमा सरलिकृत कुन्डलिनि योगको प्रारम्भ वि। सं. २०६५ मा श्री वेदाद्रि महाऋिषिद्वारा लिखित र श्री जयसिंह साह द्वारा अनुवादित ूआधुनिक समयको लागि योगू नामक पुस्तकको विमोचन पश्चात भएको मान्न सकिन्छ । केहिं समयको आवश्यक तैयारि पछि वि। सं. २०७३ को मसिर महिनामा को नामले सरलिकृत कुन्डलिनि योग केन्द्रको स्थापना भयो । भविष्यमा यस केन्द्रले जीज्ञासुवर्गमा आफ्नो सेवा पुर्याउँदै जानेछ भन्ने विश्वास लिईएको छ ।</p>
                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}

                        </article>
                    </div>
                    <div>
                        <div class="col-md-4">
                            @if($about->registration==1)
                            <a href="{{route('registration')}}" >  <button class="pull-right btn btn-lg btn-primary">आजै दर्ता गर्नुहोस् ।</button></a></div>
                        @endif
                        <div class="col-md-6">
                            @if($about->contact==1)
                            <a href="{{route('contact')}}" >  <button class="pull-right btn btn-lg btn-primary">सम्पर्क गर्नुहोस</button></a>
                      @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.kundaliniyog')

@endsection