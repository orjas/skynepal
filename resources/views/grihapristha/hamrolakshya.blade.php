@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> About</span> Green Apple</h3>
                            </div>
                            <div class="media">
                                <div class="media-left hidden-small"><img class="lazy" width="250px" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" /> </div>
                                <div class="media-body">
                                    <p>व्यक्ति(व्यक्ति मिलेर परिवार बन्दछ र परिवारहरुको समुह समाजको जन्म दिन्छ । व्यक्तिको चरित्रले उसको परिवारको चरित्र निर्धारित गर्दछ । त्यसैगरि परिवारहरुको चरित्रले समाजको चरित्र निर्धारण गर्दछ ।  एउटा शान्तचित्त मानिसले परिवारमा सजिलैसित शान्ति, समृद्धि र सुखको अवस्था ल्याउन सक्तछ । अतः शान्ति व्यक्तिमा उत्पन्न भै परिवार र तत्पश्चात समाजमा विस्तारित हुनु पर्दछ । यसै परिप्रेक्ष्यमा, सकुयो नेपालको उद्देश्य व्यक्तिगत शान्तिको अभिवृद्धिद्वारा नेपाली समाजमा शान्तिको प्रवर्द्धन गर्नु हो ।</p>
 </div>

                            </div>

                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>

                    @include('layouts.button')
                </div>
            </div>
        </div>
    </div>
    @include('layouts.kundaliniyog')



@endsection