@extends('layouts.main')
@section('content')


    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                          @foreach($teams as $team)
                            <div class="col-xs-4  mt-4"style="text-align: center">
                                <img src="{{asset('images/team/'.$team->image)}}" class="img img-circle"  width="265">
                                <div class="text" style="text-align: center"><strong>{{$team->name}}</strong><br>{{$team->position}}</div>

                            </div>




                            @endforeach

                        </article>
    </div>


    </div>
    </div>
    </div>
    </div>

@endsection