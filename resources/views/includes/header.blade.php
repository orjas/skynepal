
<header>
  <div id="headMiddle">
    <div class="container">
      <div class="row">
        <div class="col-md-2"> <a href="{{url('/')}}" class="logo wow fadeInLeft animated" title="" style="padding-top:10px;"><img class="lazy" src="{{asset("images/logo/".$setting->logo)}}" alt="" style="width:150px;"/></a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="col-md-10">
          <div id="mainMenu" class="wow fadeInRight animated" style="background-color:#2ea3ff;">
            <nav class="navbar navbar-inverse">
              <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                  <li class="{!! Request::segment(1)==''?'active':'' !!}"> <a href="{{url('/')}}"> <i class="fa fa-home"></i> गृहप्रिश्ठ</a> </li>
                  <li class="dropdown {!! Request::segment(1)=='our-team'?'active':'' !!} {!! Request::segment(1)=='about'?'active':'' !!}"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">हाम्रो <i class="fa fa-angle-down"></i> </a>
                    <ul class="dropdown-menu multi-level">
                      @foreach($abouts as $about)
                        @if($about->category=="our")
                      <li class="{!!Request::segment(2)==$about->id?"active":""  !!}"><a href="{{route('about',['id'=>$about->id])}}">{{$about->title}}</a></li>
                        @endif
                      @endforeach

                      <li class="{!!Request::segment(1)=="our-team"?"active":""  !!}"><a href="{{route('team')}}">हाम्रो टिम</a></li>
                    </ul>
                  </li>
                  <li class="dropdown {!! Request::segment(1)=='events'?'active':'' !!}  {!! Request::segment(1)=='opportunity'?'active':'' !!}"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">अवसर <i class="fa fa-angle-down"></i> </a>
                    <ul class="dropdown-menu multi-level">
                      @foreach($abouts as $about)
                        @if($about->category=="opportunity")
                          <li class="{!!Request::segment(2)==$about->id?"active":""  !!}"><a href="{{route('opportunity',['id'=>$about->id])}}">{{$about->title}}</a></li>
                        @endif
                      @endforeach

                      <li class="{!!Request::segment(1)=="events"?"active":""  !!}"><a href="{{route('events')}}">कार्यक्रम तालिका</a></li>
                    </ul>
                  </li>
                  <li class="dropdown {!! Request::segment(1)=='registration'?'active':'' !!}{!! Request::segment(1)=='participation'?'active':'' !!}"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">सहभागिता <i class="fa fa-angle-down"></i> </a>
                    <ul class="dropdown-menu multi-level">
                      @foreach($abouts as $about)
                        @if($about->category=="participation")
                          <li class="{!!Request::segment(2)==$about->id?"active":""  !!}"><a href="{{route('participation',['id'=>$about->id])}}">{{$about->title}}</a></li>
                        @endif
                      @endforeach
                      <li class="{!!Request::segment(1)=="registration"?"active":""  !!}"><a href="{{route('registration')}}">आवेदन</a></li>
                    </ul>
                  </li>
                  <li class="dropdown {!! Request::segment(1)=='speech'?'active':'' !!}
                  {!! Request::segment(1)=='news'?'active':'' !!}"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">गतिविधी <i class="fa fa-angle-down"></i> </a>
                    <ul class="dropdown-menu multi-level">
                      <li class=" {!! Request::segment(1)=='news'?'active':'' !!}"><a href="{{route('news')}}">समाचार</a></li>
                      <li class="{!! Request::segment(1)=='speech'?'active':'' !!}"><a href="{{route('speech')}}">हाम्रो मन्तब्य</a></li>
                    </ul>
                  </li>
                  
                  <li class="dropdown {!! Request::segment(1)=='media'?'active':'' !!}"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">सन्दर्भ सामाग्री <i class="fa fa-angle-down"></i> </a>
                    <ul class="dropdown-menu multi-level">
                      <li class="{!!Request::segment(1)=="media"?"active":""  !!}"><a href="{{route('media')}}">मिडिया</a></li>
                      <li><a href="#">थप जानकारी</a></li>
                      <li><a href="{{url('http://www.vethathiri.edu.in/')}}" target="_blank">English</a></li>
                    </ul>
                  </li>
                  <li class="{!! Request::segment(1)=='FAQ'?'active':'' !!}"> <a href="{{route('FAQ')}}">प्रश्नोत्तर (FAQ)</a> </li>

                  <li class="dropdown {!! Request::segment(1)=='blogs'?'active':'' !!}  {!! Request::segment(1)=='blog'?'active':'' !!}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> <i class="fa fa-angle-down"></i>विचार </a>
                    <ul class="dropdown-menu multi-level">
                      <li class="{!!Request::segment(1)=="blogs"?"active":""  !!}"><a href="{{route('blogs')}}">सङ्कलित बिचारहरु </a></li>
                      <li class="{!!Request::segment(1)=="blog"?"active":""  !!}"><a href="{{route('blog')}}">तपाईको विचार</a></li>
                    </ul>
                  </li>
                    <li class="{!! Request::segment(1)=='donation'?'active':'' !!}"> <a href="{{url('donation')}}">अनुदान</a> </li>
					  <li class="{!! Request::segment(1)=='contact-us'?'active':'' !!}"> <a href="{{route('contact')}}">संपर्क</a> </li>
                </ul>
              </div>
            
              <!--/.nav-collapse --> 
              <!-- /.navbar-collapse --> 
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
