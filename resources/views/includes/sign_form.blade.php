<div class="login-control-box">
  <div id="login_Form" class="modal fade" role="dialog">
    <div class="modal-dialog"> 
      
      <!-- Modal content-->
      <div class="modal-content" style="border-radius:0;"> 
        
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
        
        <div class="modal-body">
          <div class="row">
            <div class="col-md-5">
              <h4 class="content-title text-uppercase"><span class="gray-color"><i class="fa fa-sign-in"></i> Sign </span> In</h4>
              <form class="form-horizontal" role="form">
                <div class="form-group">
                  <div class="col-md-12">
                    <label for="model" class="control-label">User Name:</label>
                    <input placeholder="Email Id or Mobile Number" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label for="model" class="control-label">Password:</label>
                    <input placeholder="Password" class="form-control" type="password">
                  </div>
                </div>
                <button type="submit" class="btn btn-send" data-toggle="modal" data-target="#btn_unlock">Sing In <i class="fa fa-paper-plane-o"></i></button>
                <div class="clearfix"></div>
                <div class="text-right" style="margin-top:10px;"> <a href="#" class="gray-color" data-toggle="modal" data-target="#forget_email_screen">Forgot Password?</a> </div>
              </form>
            </div>
            <div class="col-md-7" style="border-left:1px solid #e5e5e5">
              <h4 class="content-title text-uppercase"><span class="gray-color"><i class="fa fa-edit"></i> Create New </span> Account</h4>
              <form class="form-horizontal" role="form">
                <div class="form-group">
                  <div class="col-md-12">
                    <label for="model" class="control-label">Full Name:</label>
                    <input placeholder="Full Name" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label for="model" class="control-label">Email:</label>
                    <input placeholder="Email" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label for="model" class="control-label">Mobile Number:</label>
                    <input placeholder="Mobile Number" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label for="Password" class="control-label">Password:</label>
                    <input placeholder="Password" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12 checkbox">
                    <label>
                      <input name="" id="" checked="" type="checkbox">
                      I have read and understood the <a href="#" data-toggle="modal" data-target="#term_cond" class="gray-color">Terms &amp; Conditions</a> and agree to it. </label>
                  </div>
                </div>
                <button type="submit" class="btn btn-send" data-toggle="modal" data-target="#btn_unlock">Sign Up <i class="fa fa-paper-plane-o"></i></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="forget_email_screen" class="modal fade" role="dialog">
    <div class="modal-dialog"> 
      
      <!-- Modal content-->
      <div class="modal-content" style="border-radius:0;"> 
        
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
        
        <div class="modal-body">
          <h4 class="content-title text-uppercase"><span class="gray-color"><i class="fa fa-edit"></i> Forgot </span> Password</h4>
          <form class="form-horizontal" role="form">
            <div class="form-group">
              <div class="col-md-12">
                <label for="model" class="control-label">Please Enter Your Email Address:</label>
                <input type="text" placeholder="Email Address" class="form-control">
              </div>
            </div>
            <button type="submit" class="btn btn-send" data-toggle="modal" data-target="#btn_unlock">Send Password <i class="fa fa-paper-plane-o"></i></button>
            <div class="clearfix"></div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div id="term_cond" class="modal fade" role="dialog">
    <div class="modal-dialog"> 
      
      <!-- Modal content-->
      <div class="modal-content" style="border-radius:0;"> 
        
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
        
        <div class="modal-body">
          <h4 class="content-title text-uppercase"><span class="gray-color"><i class="fa fa-edit"></i> Terms &amp; </span> Conditions</h4>
          <p style="color:#333;">Terms and condition</p>
        </div>
      </div>
    </div>
  </div>
</div>

