<div id="slider">
  <div id="bannerSlider" class="carousel slide carousel-fade" data-ride="carousel"> 
    <?php $i=1;?>
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      @foreach($sliders as $slider)
      <div class="item {!! $i==1?'active':'' !!}"> <img class="lazy" data-original="images/banner-slide-1.jpg" src={{asset("images/slider/".$slider->image)}} alt="" />
        <div class="carousel-caption col-md-offset-8">
          <h3>{{$slider->title}}</h3>
          <p>{{$slider->subtitle}}</p>
        </div>
      </div>
        <?php $i++;?>
      @endforeach
    </div>

    <!-- #end slider --> 
  </div>
  <div class="services wow bounceInLeft animated" style="width :300px">
    <div class="service-title">
      <h2>{{$setting->title}}</h2>
      <h4>{{$setting->subtitle}}</h4>
    </div>
    <div class="service-list clearfix">
    {!! $setting->description !!}
      <hr>
      <a href="http://www.vethathiri.edu.in/"  target="_blank" style="color: white">For more reference in English,
        <button class="btn btn-success"> Click here</button></a>

    </div>
  </div>
</div>