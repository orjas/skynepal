@extends('layouts.main')

@section('content')

    @include('includes.slider')
    <section id="about">
        <div class="container">
            <div class="row">

                @foreach($homeposts as $homepost)

                    @if($homepost->category=="Top_post")
                <div class="col-md-4 " style="min-height: 480px;">
                    <div class="module content-box  wow fadeInLeft animated">
                        <div class="main-title2">
                            <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> </span> {{$homepost->title}}</h3>
                        </div>
                        <div class="text-center">
                            <img class="lazy" data-original=""  width='300'  src="{{asset("images/homepost/".$homepost->image)}}" alt="" />
                        </div>
                        <div class="media">
                            <div class="media-left hidden-xs"> </div>
                            <div class="media-body">
                               {!! $homepost->description !!}
                            </div>
                        </div>
                    </div>
                </div>
                    @endif
                @endforeach



            </div>
            </div>
    </section>

    <!-- # EDN ABOUT SECTION -->
    <section id="top-content" class="hidden-xs  wow bounceInUp animated" style="background:#E6E6FA;">
        <div class="container">
            <div class="main-title">
                <h3>सरलिकृत कुन्डलिनी योग नै किन&nbsp;?</h3>
                <div class="seprater"></div>
            </div>
            <div class="sub-title ">
                <h4>स कु यो प्रविधी बाट योग ध्यान गर्दा त्यसको सोझो असर मन मस्तिष्क मा पर्दछ । यसले जैविक शक्ती परिचालित गरी शरिरका विभिन्न कोशीका, तन्तु तथा ग्रन्थिहरुलाई समन्वायत्मक ढगले प्रभावित गर्दछ । परिणाम स्वरुप्, मन र बुद्धी शान्त, प्रखर तथा सकारात्मक भएर आउँछ । यस परिवर्तनको प्रत्यक्ष प्रभाव मानिसको चिन्तन शैली तथा व्यवहारमा देखिन आउँछ ।&nbsp; &nbsp; ।</h4>
            </div>
            <div class="content">
                @foreach($homeposts as $homepost)
                    @if($homepost->category=="Bottom_post")
                <div class="col-md-6">
                    <div class="text-center">
                        <img class="lazy" data-original="images/businessman_tie-512.png"  width="300" src="{{asset('images/homepost/'.$homepost->image)}}" alt="" />
                    </div>
                    <div >


                        <div class="media">
                            <div class="media-left hidden-xs"></div>
                            <div class="media-body">
                                <h3 class="text-center">{{$homepost->title}}</h3>
                                <p><span>{!! $homepost->description !!}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach


            </div>
        </div>
    </section>

    <!-- # EDN TOP-CONTENT SECTION -->




    <!-- # EDN TEST-PREPRATION SECTION -->

    <section id="testimonial">
        <div class="container">
            <div class="row wow bounceInUp animated">
                <div class="col-md-12 text-center">
                    <div class="module">
                        <div class="testi-title">
                            <h3> Testimonials</h3>
                            <div class="seprater"></div>
                        </div>
                        <div id="testiCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <?php $i=0;?>
                                @foreach($testomonials as $testomonial)
                                <div class="item {!! $i==0?'active':"" !!}">


                                    <div class="media">
                                        <figure>
                                            <img class="lazy img-circle" data-original="images/noimage.png" src="{{asset('images/testomonial/'.$testomonial->image)}}" alt="" width="120" height="120" />
                                            <figcaption><span>{{$testomonial->name}}</span> {{$testomonial->position}}</figcaption>
                                        </figure>
                                    </div>
                                    <div class="testi-text">
                                     {!! $testomonial->message !!}
                                    </div>
                                </div>

                          <?php $i++;?>
                               @endforeach
                            </div>

                        </div>




                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

