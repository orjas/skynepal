<script src={{asset("assest/js/jquery.min.js")}}></script>
<script src={{asset("assest/js/bootstrap.min.js")}}></script>

<!-- gallery -->
<script src="{{asset('admin/js/notify.js')}}"></script>


<script type="text/javascript" src={{asset("assest/source/jquery.fancybox.js?v=2.1.5")}} ></script>
<link rel="stylesheet" type="text/css" href={{asset("assest/source/jquery.fancybox.css?v=2.1.5")}} media="all">
<script type="text/javascript">
    $(document).ready(function() {

        /*
         *  Button helper. Disable animations, hide close button, change title type and content
         */

        $('.fancybox-buttons').fancybox({
            openEffect  : 'none',
            closeEffect : 'none',

            prevEffect : 'none',
            nextEffect : 'none',

            closeBtn  : false,

            helpers : {
                title : {
                    type : 'inside'
                },
                buttons	: {}
            },

            afterLoad : function() {
                this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
            }
        });
    });
</script>

<!-- date -->
{{--<script src={{asset("assest/js/bootstrap-datepicker.js")}}></script>--}}
{{--<script type="text/javascript">--}}
    {{--$(document).ready(function(e) {--}}
        {{--$('#date-from').datepicker();--}}
        {{--$('#date-to').datepicker();--}}
    {{--});--}}
{{--</script>--}}
<!-- animation script -->
<script src={{asset("assest/js/wow.js")}}></script>
<script src={{asset("assest/js/custom.js")}}></script>
<!-- lazy image script -->
<script src={{asset("assest/js/jquery.lazyload.min.js")}}></script>
<script>
    $(function() {
        $("img.lazy").lazyload({
            event : "sporty"
        });
    });

    $(window).bind("load", function() {
        var timeout = setTimeout(function() { $("img.lazy").trigger("sporty") }, 5000);
    });
</script>