

<div id="innerpage">
    <div class="container">
        <div class="in-contain-wrap">
            <div class="row">
                <div class="col-md-12 wow bounceInLeft animated">
                    <article class="content-box">

                        <section id="top-content" class="hidden-xs  wow bounceInUp animated" style="background:#FBFBFB;">
                            <div class="container">
                                <div class="main-title">
                                    <h3>See What We Do?</h3>
                                    <div class="seprater"></div>
                                </div>
                                <div class="sub-title">
                                    <h4>Green Apple Inc. is the place where further studies and career <br>
                                        opportunities are pursued globally</h4>
                                </div>
                                <div class="content">
                                    <div class="col-md-3">
                                        <div class="text-center">
                                            <img class="lazy" data-original="images/businessman_tie-512.png"  width="200" src="images/businessman_tie-512.png" alt="" />
                                        </div>
                                        <div >


                                            <div class="media">
                                                <div class="media-left hidden-xs"></div>
                                                <div class="media-body">
                                                    <h3 class="text-center">Bastu sastra</h3>
                                                    <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi commodi corporis dolorem doloribus esse hic illo ipsum maiores nam nesciunt numquam odit quae, quas qui sit ullam veniam voluptatibus.</span><span>A architecto corporis deserunt enim et ex excepturi incidunt magni maxime, molestias nesciunt odio odit perferendis quisquam tempora unde, velit. A cupiditate dicta fugit iure nesciunt nisi odit reiciendis sit.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="text-center">
                                            <img class="lazy" data-original="images/businessman_tie-512.png"  width="200" src="images/businessman_tie-512.png" alt="" />
                                        </div>
                                        <div >


                                            <div class="media">
                                                <div class="media-left hidden-xs"></div>
                                                <div class="media-body">
                                                    <h3 class="text-center">Bastu sastra</h3>
                                                    <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi commodi corporis dolorem doloribus esse hic illo ipsum maiores nam nesciunt numquam odit quae, quas qui sit ullam veniam voluptatibus.</span><span>A architecto corporis deserunt enim et ex excepturi incidunt magni maxime, molestias nesciunt odio odit perferendis quisquam tempora unde, velit. A cupiditate dicta fugit iure nesciunt nisi odit reiciendis sit.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="text-center">
                                            <img class="lazy" data-original="images/businessman_tie-512.png"  width="200" src="images/businessman_tie-512.png" alt="" />
                                        </div>
                                        <div >


                                            <div class="media">
                                                <div class="media-left hidden-xs"></div>
                                                <div class="media-body">
                                                    <h3 class="text-center">Bastu sastra</h3>
                                                    <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi commodi corporis dolorem doloribus esse hic illo ipsum maiores nam nesciunt numquam odit quae, quas qui sit ullam veniam voluptatibus.</span><span>A architecto corporis deserunt enim et ex excepturi incidunt magni maxime, molestias nesciunt odio odit perferendis quisquam tempora unde, velit. A cupiditate dicta fugit iure nesciunt nisi odit reiciendis sit.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="text-center">
                                            <img class="lazy" data-original="images/businessman_tie-512.png"  width="200" src="images/businessman_tie-512.png" alt="" />
                                        </div>
                                        <div >


                                            <div class="media">
                                                <div class="media-left hidden-xs"></div>
                                                <div class="media-body">
                                                    <h3 class="text-center">Bastu sastra</h3>
                                                    <p><span>atibus.</span><span>A architecto corporis deserunt enim et ex excepturi incidunt magni maxime, molestias nesciunt odio odit perferendis quisquam tempora unde, velit. A cupiditate dicta fugit iure nesciunt nisi odit reiciendis sit.</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>

                    </article>
                </div>

            </div>
        </div>
    </div>
</div>
