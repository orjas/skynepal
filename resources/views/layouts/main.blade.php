<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{$setting->subtitle}}</title>
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset("images/logo".$setting->logo)}}">
  <link rel="manifest" href="{{asset("assest/fav/manifest.json")}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="fav/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <!-- Bootstrap -->
    @include('includes.favicon')
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

 @include('layouts.addstyle')
    {{--<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>--}}

 {!! NoCaptcha::renderJs() !!}

  <!-- facebook script -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    
    
    
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5804928b1e35c727dc04390b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
    
    
    
</head>
<body>
@include("includes.header")
<?php $message='';?>
<?php
    $message= Session::get('alert-success');
    $error_message= Session::get('error-success');
?>
@yield('content')

@include("includes.footer")
@include("layouts.addscript")
<script>

    var success="{{$message}}";
    var error_success="{{$error_message}}";

    if(success!=='') {
        $.notify(
                success,
                { className:'success',
                    globalPosition: 'top right'}
        );
    }
    if(error_success!=='') {
        $.notify(
            error_success,
            { className:'error',
                globalPosition: 'top right'}
        );
    }
</script>
</body></html>