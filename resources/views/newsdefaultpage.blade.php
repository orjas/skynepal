@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> </span>{{$news->title}}</h3>
                                <br>{{$news->date}}
                            </div>
                            <div class="media">
                                <div class="media-left hidden-small"> </div>
                                <div class="media-body">
                                    <p class=>{!!  $news->description!!}</p>
                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>
                    <div>

                    </div>
                </div>
            </div>


        </div>

    </div>





@endsection