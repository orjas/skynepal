@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="main-title2">
                            </div>
                            <div class="media">

                                <div class="col-lg-12">

                                    <div class="singlePage-collapse_wrap">
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            @foreach($questions as $question)
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title"><span> </span>
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                           href="#collapse{!! $question->id !!}" aria-expanded="true" aria-controls="collapse1">
                                                            {{$question->title}} <span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse{!! $question->id !!}" class="panel-collapse collapse " role="tabpanel"
                                                     aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                      {!! $question->description !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            @endforeach
                                            {{--<div class="panel panel-default">--}}
                                                {{--<div class="panel-heading" role="tab" id="headingTwo">--}}
                                                    {{--<h4 class="panel-title"><span> Day 02:</span>--}}
                                                        {{--<a class="collapsed" role="button" data-toggle="collapse"--}}
                                                           {{--data-parent="#accordion" href="#collapse2" aria-expanded="false"--}}
                                                           {{--aria-controls="collapse2">--}}
                                                            {{--Kathmandu-Lukla-Phakding<span><i class="fa fa-caret-down"--}}
                                                                                             {{--aria-hidden="true"></i></span>--}}
                                                        {{--</a>--}}
                                                    {{--</h4>--}}
                                                {{--</div>--}}
                                                {{--<div id="collapse2" class="panel-collapse collapse" role="tabpanel"--}}
                                                     {{--aria-labelledby="headingTwo">--}}
                                                    {{--<div class="panel-body">--}}
                                                        {{--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus--}}
                                                        {{--terry richardson ad squid. 3 wolf moon officia aute, non cupidatat--}}
                                                        {{--skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.--}}
                                                        {{--Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid--}}
                                                        {{--single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh--}}
                                                        {{--helvetica, craft beer labore wes anderson cred nesciunt sapiente ea--}}
                                                        {{--proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft--}}
                                                        {{--beer farm-to-table, raw denim aesthetic synth nesciunt you probably--}}
                                                        {{--haven't heard of them accusamus labore sustainable VHS.--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                            {{--<br>--}}
                                           {{--<br> <div class="panel panel-default">--}}
                                                {{--<div class="panel-heading" role="tab" id="headingThree">--}}
                                                    {{--<h4 class="panel-title"><span> Day 03:</span>--}}
                                                        {{--<a class="collapsed" role="button" data-toggle="collapse"--}}
                                                           {{--data-parent="#accordion" href="#collapse3" aria-expanded="false"--}}
                                                           {{--aria-controls="collapse3">--}}
                                                            {{--Phakding-Namche Bazaar.<span><i class="fa fa-caret-down"--}}
                                                                                            {{--aria-hidden="true"></i></span>--}}
                                                        {{--</a>--}}
                                                    {{--</h4>--}}
                                                {{--</div>--}}
                                                {{--<div id="collapse3" class="panel-collapse collapse" role="tabpanel"--}}
                                                     {{--aria-labelledby="headingThree">--}}
                                                    {{--<div class="panel-body">--}}
                                                        {{--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus--}}
                                                        {{--terry richardson ad squid. 3 wolf moon officia aute, non cupidatat--}}
                                                        {{--skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.--}}
                                                        {{--Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid--}}
                                                        {{--single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh--}}
                                                        {{--helvetica, craft beer labore wes anderson cred nesciunt sapiente ea--}}
                                                        {{--proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft--}}
                                                        {{--beer farm-to-table, raw denim aesthetic synth nesciunt you probably--}}
                                                        {{--haven't heard of them accusamus labore sustainable VHS.--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                           {{--<br> <div class="panel panel-default">--}}
                                                {{--<div class="panel-heading" role="tab" id="headingThree">--}}
                                                    {{--<h4 class="panel-title"><span> Day 04:</span>--}}
                                                        {{--<a class="collapsed" role="button" data-toggle="collapse"--}}
                                                           {{--data-parent="#accordion" href="#collapse4" aria-expanded="false"--}}
                                                           {{--aria-controls="collapse4">--}}
                                                            {{--Acclimatization day inNamche Bazaar.<span><i--}}
                                                                        {{--class="fa fa-caret-down" aria-hidden="true"></i></span>--}}
                                                        {{--</a>--}}
                                                    {{--</h4>--}}
                                                {{--</div>--}}
                                                {{--<div id="collapse4" class="panel-collapse collapse" role="tabpanel"--}}
                                                     {{--aria-labelledby="headingfour">--}}
                                                    {{--<div class="panel-body">--}}
                                                        {{--Acclimatization day inNamche Bazaar.--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                           {{--<br> <div class="panel panel-default">--}}
                                                {{--<div class="panel-heading" role="tab" id="headingThree">--}}
                                                    {{--<h4 class="panel-title">--}}
                                                        {{--<a class="collapsed" role="button" data-toggle="collapse"--}}
                                                           {{--data-parent="#accordion" href="#collapse5" aria-expanded="false"--}}
                                                           {{--aria-controls="collapse5">--}}
                                                            {{--<span>Cost Includes</span><span><i class="fa fa-caret-down"--}}
                                                                                               {{--aria-hidden="true"></i></span>--}}
                                                        {{--</a>--}}
                                                    {{--</h4>--}}
                                                {{--</div>--}}
                                                {{--<div id="collapse5" class="panel-collapse collapse" role="tabpanel"--}}
                                                     {{--aria-labelledby="headingfive">--}}
                                                    {{--<div class="panel-body">--}}
                                                        {{--<ul>--}}
                                                            {{--<li>3 nights hotel in Kathmandu with bed & breakfast</li>--}}
                                                            {{--<li> All sightseeing with guide in Kathmandu valley</li>--}}
                                                            {{--<li>trekking Permit fee of Everest region</li>--}}
                                                            {{--<li>Meal: All meal Breakfast, Lunch, dinner during the trek best--}}
                                                                {{--available restaurant. Each meal 1 big pot tea every 5 person &--}}
                                                                {{--if less then 5 persons, small pot tea (1 Big pot will be 10--}}
                                                                {{--cups)--}}
                                                            {{--</li>--}}
                                                            {{--<li>One experience trekking guide 1 porter for 2 trekker (each--}}
                                                                {{--member 15KG)--}}
                                                            {{--</li>--}}
                                                            {{--<li>Everest National Park permit fee.</li>--}}
                                                            {{--<li>Flight Kathmandu - Lukla - Kathmandu.</li>--}}
                                                            {{--<li>Insurance for Guide & Porter</li>--}}
                                                            {{--<li>All accommodation during the trek best available--}}
                                                                {{--hotel/guesthouses--}}
                                                            {{--</li>--}}
                                                            {{--<li> Airport -Hotel-Airport transfer</li>--}}
                                                            {{--<li>Our service charge</li>--}}
                                                        {{--</ul>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                           {{--<br> <div class="panel panel-default">--}}
                                                {{--<div class="panel-heading" role="tab" id="headingThree">--}}
                                                    {{--<h4 class="panel-title">--}}
                                                        {{--<a class="collapsed" role="button" data-toggle="collapse"--}}
                                                           {{--data-parent="#accordion" href="#collapse6" aria-expanded="false"--}}
                                                           {{--aria-controls="collapse6">--}}
                                                            {{--<span>Cost Excludes</span><span><i--}}
                                                                        {{--class="fa fa-caret-down" aria-hidden="true"></i></span>--}}
                                                        {{--</a>--}}
                                                    {{--</h4>--}}
                                                {{--</div>--}}
                                                {{--<div id="collapse6" class="panel-collapse collapse" role="tabpanel"--}}
                                                     {{--aria-labelledby="headingsix">--}}
                                                    {{--<div class="panel-body">--}}
                                                        {{--<ul>--}}
                                                            {{--<li>PERSONAL equipment for trekking</li>--}}
                                                            {{--<li>Personal Insurance for travel to Nepal</li>--}}
                                                            {{--<li>Lunch & dinner in Kathmandu</li>--}}
                                                            {{--<li>Extra meal except breakfast, lunch and dinner as well as tea /--}}
                                                                {{--coffee cup or pot.--}}
                                                            {{--</li>--}}
                                                            {{--<li>Emergency Rescue evacuation during the trek incase needed for--}}
                                                                {{--trekker--}}
                                                            {{--</li>--}}
                                                            {{--<li>If bad whether and use Lukla - Kathmandu will be cost around USD--}}
                                                                {{--600--}}
                                                            {{--</li>--}}
                                                            {{--<li>Personal expenses</li>--}}
                                                            {{--<li>Tipping (Tips)</li>--}}
                                                            {{--<li>Bar bills, Beverage and mineral water during the trek.</li>--}}
                                                        {{--</ul>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection