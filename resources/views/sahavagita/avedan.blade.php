@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="media">
                                <div class="media-body"><p></p>

                            </div>
                                <div id="innerpage">
                                    <div class="container">
                                        <div class="in-contain-wrap">
                                            <div class="row">
                                                <div class="col-md-8 wow bounceInLeft animated">
                                                    <article>
                                                        <div class="main-title2">
                                                            <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> आवेदन </span>फारम</h3>
                                                            <span><br>स. कु. यो. नेपाल (SKY/Nepal) मार्फत योग सिक्न चाहने जीज्ञासुले भर्नु पर्ने आवेदन फारम</span>
                                                        </div>
                                                       {!! Form::open(['url' => '/registration', 'method' => 'post','files'=>true]) !!}


                                                            <fieldset>
                                                                <legend> </legend>
                                                                <div class="row height50">

                                                                    <div class="col-md-12">
                                                                        <div class="form-group {{ $errors->has('course') ? ' has-error' : '' }}">
                                                                            <label style="display:block;">कोर्सको उद्देश्य</label>


                              <span>
                      <input type="checkbox" name="course[]" value="ध्यान"/>
                        (क) ध्यान</span>
                                                                            <span>
                      <input type="checkbox" name="course[]" value="आत्म निरीक्षण "/>
                      (ख) आत्म निरीक्षण </span>
                                                                            <span>
                      <input type="checkbox" name="course[]" value=" काया-कल्प "/>
                     (ग) काया-कल्प  </span>
                              @if ($errors->has('course'))
                                  <span class="help-block">
                                        <strong>{{ $errors->first('course') }}</strong>
                                    </span>
                              @endif

                                                                                                                </div>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                                            <label>नाम</label>
                                                                            <input class="form-control" name="name" id="name" placeholder="" type="text">
                                                                            @if ($errors->has('name'))
                                                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label>ईमेल</label>
                                                                            <input class="form-control" name="email" id="email" placeholder="" type="email">
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                                            <label>फोन</label>
                                                                            <input class="form-control" name="phone" id="phone" placeholder="" type="text">
                                                                            @if ($errors->has('phone'))
                                                                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                                                            @endif
                                                                        </div>
                                                                    </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label >लिङ्ग</label>
                                                                        <select name="gender" class="form-control">
                                                                            <option value="female">महिला</option>
                                                                            <option value="male">पुरुष</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group {{ $errors->has('age') ? ' has-error' : '' }}">
                                                                        <label>उमेर</label>
                                                                        <input class="form-control" name="age" id="phone" placeholder="" type="number">
                                                                        @if ($errors->has('age'))
                                                                            <span class="help-block">
                                        <strong>{{ $errors->first('age') }}</strong>
                                    </span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                                                        <label>ठेगाना</label>
                                                                        <input class="form-control" name="address" id="phone" placeholder="" type="text">
                                                                        @if ($errors->has('address'))
                                                                            <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                                                        @endif
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>शिक्षा</label>
                                                                        <input class="form-control" name="education" id="phone" placeholder="" type="text">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>व्यवशाय</label>
                                                                        <input class="form-control" name="profession" id="phone" placeholder="" type="text">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>तस्विर</label>
                                                                        <input class="form-control" name="image" id="phone" placeholder="" type="file">
                                                                    </div>
                                                                </div>


                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>म शारिरिक तथा मानसिक रुपले स्वस्थ छु । छैन भने विवरण दिनुहोस</label>
                                                                        <input class="form-control" name="physical" id="phone" placeholder="" type="text">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>

                                                                            यस अघि स. कु. यो. नेपाल बाट लिएको कोर्सको उद्देश्य र मिति</label>
                                                                        <input class="form-control" name="recent_course" id="phone" placeholder="" type="text">
                                                                    </div>
                                                                </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                 {{--<div id="Recaptcha2"></div>--}}
                                                                    {{--{!! NoCaptcha::renderJs() !!}--}}

                                                                    {!! app('captcha')->display() !!}
                                                                    {{--<div class="g-recaptcha" data-sitekey="{{(config('googlekey.key'))}}"></div>--}}

                                                                    <div>
                                                                        @if ($errors->has('g-recaptcha-response'))

                                                                            <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>

                                                                        @endif

                                                                    </div>


                                                                    <button type="submit" class="btn btn-success pull-right" id="btnContactUs">
                                                                        Send Message</button>
                                                                </div>

                                                            </fieldset>
                                                            {{--<div class="text-right">--}}
                                                                {{--<button class="btn btn-send" type="submit">Send <i class="fa fa-paper-plane-o"></i> </button>--}}
                                                            {{--</div>--}}
                                                        {!! Form::close() !!}
                                                    </article>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
</div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}   </article>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {{--<script type="text/javascript">--}}
        {{--var CaptchaCallback = function() {--}}
            {{--grecaptcha.render('Recaptcha2', {'sitekey' : '6Lc92z4UAAAAABjNc4S1OhXFFfFMaXlrkXSIHhf4'});--}}
{{--//            grecaptcha.render('RecaptchaField2', {'sitekey' : '6Lc_your_site_key'});--}}
        {{--};--}}
    {{--</script>--}}

@endsection