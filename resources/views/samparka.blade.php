@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">
                            <div class="media">
                                <div class="media-body">
                                    <fieldset>
                                        <legend>Contact Us</legend>
                                        <div class="row height50">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label></label>
                                            {!! Form::open(['url' => '/contact-us', 'method' => 'post']) !!}
                                            	
                                          
                                            <input class="form-control" name="name" id="name" placeholder="Name" type="text" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label></label>
                                            <input class="form-control" name="email" id="email" placeholder="Email" type="email" required>
                                        </div>
                                    </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label></label>
                                                    <textarea rows="5" name="message" placeholder="message" class="form-control" required></textarea>
                                                    {{--<input class="form-control" name="message" id="message" placeholder="Message" type="text" required>--}}
                                                </div>
                                            </div>


                                            <div class="col-md-12 text-center" >
                                                <input type="text" name="check" hidden >
                                                @if ($errors->has('check'))

                                                <span class="help-block">
                                                <strong>{{ $errors->first('check') }}</strong>
                                                </span>

                                                @endif
                                                 {{--google Captcha--}}
                                                {{--{!! app('captcha')->display() !!}--}}
                                                {{--<div>--}}
                                                    {{--@if ($errors->has('g-recaptcha-response'))--}}

                                                        {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('g-recaptcha-response') }}</strong>--}}
                                    {{--</span>--}}

                                                    {{--@endif--}}

                                                {{--</div>--}}


                                                <button type="submit" class="btn btn-success " id="btnContactUs">
                                                    Send Message</button>
                                            </div>
                                            {!! csrf_field() !!}
                                            {!! Form::close() !!}


                                        </div >
                                        </fieldset>
                                </div>
                            </div>
                              </article>
                    </div>

                    <div class="col-md-12 wow bounceInLeft animated">
                        <article class="content-box">

                            <div class="media">
                                <div class="media-body">
                                    <fieldset>
                                        <legend>We are located at</legend>
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14132.504394531226!2d85.27487342657089!3d27.68249726293056!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjfCsDQxJzA5LjEiTiA4NcKwMTYnNTAuNCJF!5e0!3m2!1sen!2snp!4v1517811821563"
                                                width="1200" height="350" frameborder="0" style="border:0"
                                                allowfullscreen></iframe>
                                    </fieldset>
                                </div>
                            </div>
                             </article>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var CaptchaCallback = function() {
            grecaptcha.render('RecaptchaField1', {'sitekey' : '6Lc92z4UAAAAABjNc4S1OhXFFfFMaXlrkXSIHhf4'});
//            grecaptcha.render('RecaptchaField2', {'sitekey' : '6Lc_your_site_key'});
        };
    </script>

@endsection