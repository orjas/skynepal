@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 ">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> Our</span>Media</h3>
                            </div>
                            <div class="media">
                                <div class="media-body">

                    <!-- /.col-lg-4 -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <!-- /.panel-heading -->
                                                <div class="panel-body">
                                                    <!-- Nav tabs -->
                                                    <ul class="nav nav-tabs">
                                                        <li ><a href="{{route('media')}}" >फोटो </a>
                                                        </li>
                                                        <li><a href="{{route('video')}}" >भिडियो</a>
                                                        </li>
                                                        <li class="active"><a href="{{route('content')}}" >लिखित सामाग्री</a>
                                                        </li>
                                                    </ul>

                                                    <!-- Tab panes -->
                                                    {{--<div class="tab-content">--}}


                                                        <div class="tab-pane fade in active" id="messages">

                                                            <br>
                                                          <ul>
                                                              @forelse($contents as $content)
                                                              <li style="color: red"><a href="{{asset('file/'.$content->file)}} " target="_blank"><span class='fa fa-file-{!! $content->extension !!}-o fa-lg'></span> {{$content->name}}</a></li><br>
                                                         @empty
                                                                  <p>Nothing to show</p>
                                                          @endforelse
                                                          </ul>
                                                        </div>
                                                    {{--</div>--}}
                                                <!-- /.panel-body -->
                                            </div>
                                            <!-- /.panel -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <!-- /.col-lg-6 -->
                                    </div>
                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}
                        </div>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection