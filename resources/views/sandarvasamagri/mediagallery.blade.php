@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 ">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> Our</span>Media</h3>
                            </div>
                            <div class="media">
                                <div class="media-body">

                    <!-- /.col-lg-4 -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <!-- /.panel-heading -->
                                                <div class="panel-body">
                                                    <!-- Nav tabs -->


                                                    <ul class="nav nav-tabs">
                                                        <li  class="active"><a href="{{route('media')}}" >फोटो </a>
                                                        </li>
                                                        <li><a href="{{route('video')}}" >भिडियो</a>
                                                        </li>
                                                        <li><a href="{{route('content')}}" >लिखित सामाग्री</a>
                                                        </li>
                                                    </ul>

                                                    <!-- Tab panes -->
                                                    {{--<div class="tab-content">--}}
                                                        <div class="tab-pane fade in active" id="home">
                                                       <br>
                                                           @forelse($galleries as $gallery)
                                                               <div class="col-md-4" style="min-height:250px ">
                                                               <img src="{{asset('images/gallery/'.$gallery->image)}}" width="300" height="150">
                                                                   <br><strong>{{$gallery->description}}</strong>
                                                               </div>
                                                               @empty
                                                               <p>Nothing to show</p>
                                                                   @endforelse

                                                        </div>

                                            {{--</div>--}}
                                            <!-- /.panel -->
                                        </div>
                                        <!-- /.col-lg-6 -->
                                        <!-- /.col-lg-6 -->
                                    </div>
                                </div>
                            </div>
                            {{--<h4>Who we are</h4>--}}
                            {{--<img class="lazy" data-original="images/geenappcap.png" src="{{asset("assest/images/geenappcap.png")}}" alt="" />--}}
                                </div>
                            </div>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection