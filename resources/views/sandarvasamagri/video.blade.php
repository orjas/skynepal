@extends('layouts.main')
@section('content')
    <div id="innerpage">
        <div class="container">
            <div class="in-contain-wrap">
                <div class="row">
                    <div class="col-md-12 ">
                        <article class="content-box">
                            <div class="main-title2">
                                <h3> <span><i class="fa fa-file-text-o" aria-hidden="true"></i> Our</span>Media</h3>
                            </div>
                            <div class="media">
                                <div class="media-body">

                    <!-- /.col-lg-4 -->
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default">
                                                <!-- /.panel-heading -->
                                                <div class="panel-body">
                                                    <!-- Nav tabs -->

                                                    <ul class="nav nav-tabs">
                                                        <li ><a href="{{route('media')}}" >फोटो </a>
                                                        </li>
                                                        <li class="active"><a href="{{route('video')}}" >भिडियो</a>
                                                        </li>
                                                        <li ><a href="{{route('content')}}" >लिखित सामाग्री</a>
                                                        </li>
                                                    </ul>

                                                    <!-- Tab panes -->

                                                    {{--<div class="tab-content">--}}

                                                        <div class="tab-pane fade in active" >

                                                            <div class="col-md-12"><br>

                                                                @forelse($videos as $video)
                                                                <div class="col-md-6 mt-3 ">
                                                                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/{{$video->video}}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                                                </div>

                                                                @empty
                                                                    <p>Nothing to show</p>
                                                                @endforelse

                                                            </div>
                                                        </div>

                                                    {{--</div>--}}
                                                    <!-- /.panel-body -->
                                                </div>
                                                <!-- /.panel -->
                                            </div>
                                            <!-- /.col-lg-6 -->
                                            <!-- /.col-lg-6 -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                              </article>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection